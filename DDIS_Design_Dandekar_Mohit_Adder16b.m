%% initiate the simulation

%Mohit Dandekar r0603366


clear all;
inputfile = 'DDIS_Design_Dandekar_Mohit_Adder16b_BrentKung';
clear transientsim
clear acsim
close all

if (exist('supply', 'var') == 0)
    supply = .9;
end

%Flags to control analysis of additional tests and display of 
%switching activity
analysis = 1; animate=1;

globals.supply = supply;
globals.gamma = 1;


if(analysis == 0)
    simtime = 16e-9; %16ns original simulation time
else 
    simtime = 188e-9; %extended tests
end
globals.sim_time = simtime;

tic;
runSpice;
toc
%% calculate the characteristics

vdd = globals.supply;

%- delay calculation -
%---------------------
time        = evalsig(transientsim, 'TIME');
b0          = evalsig(transientsim, 'b_buff0');

b0Crossing  = findPositiveZeroCrossings(time,b0-vdd/2);
for i = 0:16
    signal = evalsig(transientsim, [ 's',num2str(i)]);
    sCrossP{i+1} = findPositiveZeroCrossings(time, signal-vdd/2);
    sCrossN{i+1} = findNegativeZeroCrossings(time, signal-vdd/2);
    sum{i+1} = signal;
end

delay = (sCrossP{15+1}(1) - b0Crossing(1));

%- Power calculations -
%----------------------
I_vdd   = evalsig(transientsim, 'I_vdd');
n = 3; % amount of patterns simulated
Charge = zeros(n,1);
for i = 1:n
    if i == 1
        begintime = 0;
        endtime = 4e-9*i-0.2e-9;
    else
        begintime = 4e-9*(i-1)-0.2e-9;
        endtime = 4e-9*i-0.2e-9;
    end
    if begintime == 0
        beginindex = 1;
    else
        beginindex = getElementNumber(time,begintime);
    end
    endindex = getElementNumber(time,endtime);
    Charge(i) = trapz(time(beginindex:endindex),I_vdd(beginindex:endindex));
    
end

Energy = -Charge*vdd;
SwitchingEnergyWC = Energy(3); % energy of the switching event
DCpower = Energy(2)/4e-9; % power = energy/time


%- Output generation -
%---------------------
if( exist('displayOn','var') == 0)
    displayOn = 1;
end

if(displayOn)
    disp(' ');
    disp(['Worst Case delay            = ' num2str(delay*1e12),' ps'])
    %disp(' ')
    disp(['Worst Case Switching energy  = ',num2str(SwitchingEnergyWC*1e15),' fJ'])
    
    %disp(' ')
    disp(['DC power consumption         = ',num2str(DCpower*1e9),' nW'])
    
    type(['spicefiles/',inputfile, '.err0'])
end




if(analysis==1)
    
    figure('units','normalized','outerposition',[0 0 1 1]);
    plotsig(transientsim,'a_buff0;s0,s1,s2,s3;s4,s5,s6,s7;s8,s9,s10,s11;s12,s13,s14,s15,s16')
    figure('units','normalized','outerposition',[0 0 1 1]);
    plotsig(transientsim,'xadder_gen1_0,xadder_genb3_0,xadder_gen7_0,xadder_genb11_0,xadder_gen13_0,xadder_genb14_0,s15;xadder_prop1_0,xadder_propb3_0,xadder_prop7_0,xadder_propb11_0,xadder_prop13_0,xadder_propb14_0,s15')


    a0 = evalsig(transientsim, 'a_buff0');

    delaycarry=zeros(17,1);
    outsig_list = {'b_buff0','xadder_gen0xbuf0','xadder_gen1_0xbuf1'...
                   'xadder_gen2_0','xadder_gen3_0xbuf2','xadder_gen4_0',...    
                   'xadder_gen5_0xbuf0','xadder_gen6_0','xadder_genb7_0xbuf2',...
                   'xadder_genb8_0','xadder_genb9_0xbuf0','xadder_genb10_0','xadder_genb11_0xbuf1',...
                   'xadder_genb12_0','xadder_genb13_0xbuf0','xadder_genb14_0','xadder_genb15_0'};
    for i=0:16
        sig = evalsig(transientsim,outsig_list{i+1});
        begintime = (44 + 8*i)*1e-9 - 0.2e-9;
        endtime = (44 + 8*i + 4)*1e-9 - 0.2e-9;
        beginindex = getElementNumber(time,begintime);
        endindex = getElementNumber(time,endtime);

        sCrossPTime = findPositiveZeroCrossings(time(beginindex:endindex), sig(beginindex:endindex)-vdd/2);
        sCrossNTime = findNegativeZeroCrossings(time(beginindex:endindex), sig(beginindex:endindex)-vdd/2);
        if(isempty(sCrossPTime) && isempty(sCrossNTime))
            continue;
        elseif(isempty(sCrossPTime) && ~isempty(sCrossNTime))
            sCrossTime = sCrossNTime(end);
        elseif(~isempty(sCrossPTime) && isempty(sCrossNTime))
            sCrossTime = sCrossPTime(end);
        else
            sCrossTime = max(sCrossNTime(end),sCrossPTime(end));
        end


        aCrossPTime = findPositiveZeroCrossings(time(beginindex:endindex), a0(beginindex:endindex)-vdd/2);
        delaycarry(i+1) = sCrossTime-aCrossPTime;
    end



    delaybeforemux=zeros(17,1);
    delaybeforemuxb=zeros(17,1);

    for i=1:15

        sig = evalsig(transientsim,['xadder_xsum_',num2str(i),'_out']);
        sigb = evalsig(transientsim,['xadder_xsum_',num2str(i),'_outb']);

        begintime = (180)*1e-9 - 0.2e-9;
        endtime =   (184)*1e-9 - 0.2e-9;
        beginindex = getElementNumber(time,begintime);
        endindex = getElementNumber(time,endtime);

        sCrossPTime = findPositiveZeroCrossings(time(beginindex:endindex), sig(beginindex:endindex)-vdd/2);
        sCrossNTime = findNegativeZeroCrossings(time(beginindex:endindex), sigb(beginindex:endindex)-vdd/2);
        aCrossPTime = findPositiveZeroCrossings(time(beginindex:endindex), a0(beginindex:endindex)-vdd/2);
        delaybeforemux(i+1) = sCrossPTime-aCrossPTime;
        delaybeforemuxb(i+1) = sCrossNTime-aCrossPTime;
    end





    delayatbit=zeros(17,1);

    for i=0:16
        sig = evalsig(transientsim,['s',num2str(i)]);
        begintime = (44 + 8*i)*1e-9 - 0.2e-9;
        endtime = (44 + 8*i + 4)*1e-9 - 0.2e-9;
        beginindex = getElementNumber(time,begintime);
        endindex = getElementNumber(time,endtime);
        sCrossPTime = findPositiveZeroCrossings(time(beginindex:endindex), sig(beginindex:endindex)-vdd/2);
        aCrossPTime = findPositiveZeroCrossings(time(beginindex:endindex), a0(beginindex:endindex)-vdd/2);
        delayatbit(i+1) = sCrossPTime-aCrossPTime;
    end

    figure('units','normalized','outerposition',[0 0 1 1]);hold on;
    bar([0:1:16]-0.2,delaybeforemux*1e12,0.2,'r');
    bar([0:1:16]    ,delaybeforemux*1e12,0.2,'g');
    bar([0:1:16]+0.2,[delaycarry*1e12, (delayatbit-delaycarry)*1e12],0.2,'stacked');
    [ymax,xmax] = max(delayatbit*1e12);
    strmax = ['Max Delay To Sum = ',num2str(ymax),'(ps) at bit: ',num2str(xmax-1),'\rightarrow'];
    text((xmax-1),ymax*1.01,strmax,'HorizontalAlignment','right');

    [ymax,xmax] = max(delaycarry*1e12);
    strmax = ['Max Delay for Carry= ',num2str(ymax),'(ps) at bit: ',num2str(xmax-1)];
    text((xmax+1),ymax*1.7,strmax,'HorizontalAlignment','right');
    quiver( xmax+1,ymax*1.7,-1.8,-0.7*ymax ,0,'.b')
    plot([-1:1:18],650*ones(20),'r-','Linewidth',3)
    legend('Delay for Input XOR','Delay for Input XNOR','Delay for Carry','Delay for Muxing','Location','northwest')
    xlabel('Bit Location'); ylabel('Delay from a0 to targetted sum bit (ps)')
    hold off





    NT = length(time);
    activitymap = zeros(6*3,9,NT);
    topology = {'a_buff0' ,'xadder_genb0'  ,'xadder_gen0xbuf0'  ,'xadder_gen0xbuf0'      ,'xadder_gen0xbuf0'      ,'xadder_gen0xbuf0'      ,'xadder_gen0xbuf0'      ,'xadder_gen0xbuf0'      ,'s1';...
                'b_buff0' ,'xadder_propb0' ,'xadder_prop0xbuf0' ,'xadder_prop0xbuf0'     ,'xadder_prop0xbuf0'     ,'xadder_prop0xbuf0'     ,'xadder_prop0xbuf0'     ,'0'                     ,'s1';...
                'a_buff1' ,'xadder_genb1'  ,'xadder_gen1_0'     ,'xadder_genb1_0xbuf0'   ,'xadder_gen1_0xbuf1'    ,'xadder_gen1_0xbuf1'    ,'xadder_gen1_0xbuf1'    ,'xadder_gen1_0xbuf1'    ,'s2';...
                'b_buff1' ,'xadder_propb1' ,'xadder_prop1_0'    ,'xadder_propb1_0xbuf0'  ,'0'                     ,'0'                     ,'0'                     ,'0'                     ,'s2';...
                'a_buff2' ,'xadder_genb2'  ,'xadder_gen2xbuf0'  ,'xadder_genb2xbuf1'     ,'xadder_gen2_0'         ,'xadder_gen2_0'         ,'xadder_gen2_0'         ,'xadder_gen2_0'         ,'s3';...
                'b_buff2' ,'xadder_propb2' ,'xadder_prop2xbuf0' ,'xadder_propb2xbuf1'    ,'xadder_prop2_0'        ,'0'                     ,'0'                     ,'0'                     ,'s3';...
                'a_buff3' ,'xadder_genb3'  ,'xadder_gen3_2'     ,'xadder_genb3_0'        ,'xadder_gen3_0xbuf0'    ,'xadder_genb3_0xbuf1'   ,'xadder_gen3_0xbuf2'    ,'xadder_gen3_0xbuf2'    ,'s4';...
                'b_buff3' ,'xadder_propb3' ,'xadder_prop3_2'    ,'xadder_propb3_0'       ,'xadder_prop3_0xbuf0'   ,'xadder_propb3_0xbuf1'  ,'0'                     ,'0'                     ,'s4';...
                'a_buff4' ,'xadder_genb4'  ,'xadder_gen4xbuf0'  ,'xadder_genb4xbuf1'     ,'xadder_genb4xbuf1'     ,'xadder_genb4xbuf1'     ,'xadder_gen4_0'         ,'xadder_gen4_0'         ,'s5';...
                'b_buff4' ,'xadder_propb4' ,'xadder_prop4xbuf0' ,'xadder_propb4xbuf1'    ,'xadder_propb4xbuf1'    ,'xadder_propb4xbuf1'    ,'xadder_prop4_0'        ,'0'                     ,'s5';...            
                'a_buff5' ,'xadder_genb5'  ,'xadder_gen5_4'     ,'xadder_genb5_4xbuf0'   ,'xadder_gen5_4xbuf1'    ,'xadder_genb5_0'        ,'xadder_gen5_0xbuf0'    ,'xadder_gen5_0xbuf0'    ,'s6';...
                'b_buff5' ,'xadder_propb5' ,'xadder_prop5_4'    ,'xadder_propb5_4xbuf0'  ,'xadder_prop5_4xbuf1'   ,'xadder_propb5_0'       ,'0'                     ,'0'                     ,'s6';...
                'a_buff6' ,'xadder_genb6'  ,'xadder_gen6xbuf0'  ,'xadder_genb6xbuf1'     ,'xadder_genb6xbuf1'     ,'xadder_genb6xbuf1'     ,'xadder_gen6_0'         ,'xadder_gen6_0'         ,'s7';...
                'b_buff6' ,'xadder_propb6' ,'xadder_prop6xbuf0' ,'xadder_propb6xbuf1'    ,'xadder_propb6xbuf1'    ,'xadder_propb6xbuf1'    ,'xadder_prop6_0'        ,'0'                     ,'s7';...
                'a_buff7' ,'xadder_genb7'  ,'xadder_gen7_6'     ,'xadder_genb7_4'        ,'xadder_gen7_0'         ,'xadder_genb7_0xbuf0'   ,'xadder_gen7_0xbuf1'    ,'xadder_genb7_0xbuf2'   ,'s8';...
                'b_buff7' ,'xadder_propb7' ,'xadder_prop7_6'    ,'xadder_propb7_4'       ,'xadder_prop7_0'        ,'xadder_propb7_0xbuf0'  ,'xadder_prop7_0xbuf1'   ,'0'                     ,'s8';...
                'a_buff8' ,'xadder_genb8'  ,'xadder_gen8xbuf0'  ,'xadder_gen8xbuf0'      ,'xadder_gen8xbuf0'      ,'xadder_gen8xbuf0'      ,'xadder_gen8xbuf0'      ,'xadder_genb8_0'        ,'s9';...
                'b_buff8' ,'xadder_propb8' ,'xadder_prop8xbuf0' ,'xadder_prop8xbuf0'     ,'xadder_prop8xbuf0'     ,'xadder_prop8xbuf0'     ,'xadder_prop8xbuf0'     ,'0'                     ,'s9';...
                'a_buff9' ,'xadder_genb9'  ,'xadder_gen9_8'     ,'xadder_genb9_8xbuf0'   ,'xadder_genb9_8xbuf0'   ,'xadder_genb9_8xbuf0'   ,'xadder_gen9_0'         ,'xadder_genb9_0xbuf0'   ,'s10';...
                'b_buff9' ,'xadder_propb9' ,'xadder_prop9_8'    ,'xadder_propb9_8xbuf0'  ,'xadder_propb9_8xbuf0'  ,'xadder_propb9_8xbuf0'  ,'xadder_prop9_0'        ,'0'                     ,'s10';...
                'a_buff10','xadder_genb10' ,'xadder_gen10xbuf0' ,'xadder_gen10xbuf0'     ,'xadder_gen10xbuf0'     ,'xadder_gen10xbuf0'     ,'xadder_gen10xbuf0'     ,'xadder_genb10_0'       ,'s11';...
                'b_buff10','xadder_propb10','xadder_prop10xbuf0','xadder_prop10xbuf0'    ,'xadder_prop10xbuf0'    ,'xadder_prop10xbuf0'    ,'xadder_prop10xbuf0'    ,'0'                     ,'s11';...
                'a_buff11','xadder_genb11' ,'xadder_gen11_10'   ,'xadder_genb11_8'       ,'xadder_gen11_8xbuf0'   ,'xadder_genb11_0'       ,'xadder_gen11_0xbuf0'   ,'xadder_genb11_0xbuf1'  ,'s12';...
                'b_buff11','xadder_propb11','xadder_prop11_10'  ,'xadder_propb11_8'      ,'xadder_prop11_8xbuf0'  ,'xadder_propb11_0'      ,'xadder_prop11_0xbuf0'  ,'0'                     ,'s12';...
                'a_buff12','xadder_genb12' ,'xadder_gen12xbuf0' ,'xadder_gen12xbuf0'     ,'xadder_gen12xbuf0'     ,'xadder_gen12xbuf0'     ,'xadder_gen12xbuf0'     ,'xadder_genb12_0'       ,'s13';...
                'b_buff12','xadder_propb12','xadder_prop12xbuf0','xadder_prop12xbuf0'    ,'xadder_prop12xbuf0'    ,'xadder_prop12xbuf0'    ,'xadder_prop12xbuf0'    ,'0'                     ,'s13';...
                'a_buff13','xadder_genb13' ,'xadder_gen13_12'   ,'xadder_genb13_12xbuf0' ,'xadder_genb13_12xbuf0' ,'xadder_genb13_12xbuf0' ,'xadder_gen13_0'        ,'xadder_genb13_0xbuf0'  ,'s14';...
                'b_buff13','xadder_propb13','xadder_prop13_12'  ,'xadder_propb13_12xbuf0','xadder_propb13_12xbuf0','xadder_propb13_12xbuf0','xadder_prop13_0'       ,'0'                     ,'s14';...
                'a_buff14','xadder_genb14' ,'xadder_gen14xbuf0' ,'xadder_gen14xbuf0'     ,'xadder_gen14xbuf0'     ,'xadder_gen14xbuf0'     ,'xadder_gen14xbuf0'     ,'xadder_genb14_0'       ,'s15';...
                'b_buff14','xadder_propb14','xadder_prop14xbuf0','xadder_prop14xbuf0'    ,'xadder_prop14xbuf0'    ,'xadder_prop14xbuf0'    ,'xadder_prop14xbuf0'    ,'0'                     ,'s15';...
                'a_buff15','xadder_genb15' ,'xadder_gen15_14'   ,'xadder_genb15_12'      ,'xadder_gen15_8'        ,'xadder_gen15_8'        ,'xadder_gen15_8'        ,'xadder_genb15_0'       ,'s16';...
                'b_buff15','xadder_propb15','xadder_prop15_14'  ,'xadder_propb15_12'     ,'xadder_prop15_8'       ,'xadder_prop15_8'       ,'xadder_prop15_8'       ,'0'                     ,'s16'  };

    num_nodes = 16;
    for layer=1:9
       for node = 0:(num_nodes-1)
           name1 = topology{node*2+1 ,layer};
           name2 = topology{node*2+2 ,layer};
           activitymap(node*3 +1,layer,:) = evalsig(transientsim,name1);
           activitymap(node*3 +2,layer,:) = evalsig(transientsim,name2);
       end
    end

    if(animate==1)
        f = figure('units','normalized','outerposition',[0 0 1 1]);
        figure(f)
        atext = ['a0 ';'a1 ';'a2 ';'a3 ';'a4 ';'a5 ';'a6 ';'a7 ';'a8 ';'a9 ';'a10';'a11';'a12';'a13';'a14';'a15'];
        btext = ['b0 ';'b1 ';'b2 ';'b3 ';'b4 ';'b5 ';'b6 ';'b7 ';'b8 ';'b9 ';'b10';'b11';'b12';'a13';'b14';'b15'];
        stext = ['s1 ';'s2 ';'s3 ';'s4 ';'s5 ';'s6 ';'s7 ';'s8 ';'s9 ';'s10';'s11';'s12';'s13';'s14';'s15';'s16'];
        begintime = 48e-9;
        for t=getElementNumber(time,begintime):4:NT

            figure(f);
            %bar3([0:1:8],transpose(activitymap(:,:,t)),0.25)

            imagesc(transpose(activitymap(1:3*(num_nodes)-1,:,t)))
            view(180,-90)
            hText = text(8,0, ['Time: ',num2str(time(t)*1e9),' (ns)/',num2str(time(end)*1e9),' (ns)']);
            text([0:1:(num_nodes-1)]*3+1.25,ones(num_nodes,1),atext) ;text([0:1:(num_nodes-1)]*3+2.25,ones(num_nodes,1),btext) ; text([0:1:(num_nodes-1)]*3+1.75,9*ones(num_nodes,1),stext)  ;

            drawnow
            delete(hText);
        end
    end

end


