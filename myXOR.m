close all
clear all
clear transientsim
clear acsim
inputfile = 'myXOR';

globals.load_size = 4;
globals.supply = .7;
globals.gate_size = 1;
globals.gamma = 2;


runSpice;
    
%plotsig(transientsim, 'a,b,xxor2_n12,xxor2_n34,xxor2_n78,xxor2_n910;c1,c2,c3');

time        = evalsig(transientsim, 'TIME');
N = 12; % amount of patterns simulated
T = 4e-9; % period of pattern

%input pattern 
inp = [0    0 ; ...
       0    1 ; ...
       0    0 ; ...
       1    0 ; ...
       0    0 ; ...
       1    1 ; ...
       0    1 ; ...
       1    0 ; ...
       1    1 ; ...
       1    0 ; ...
       0    1 ; ...
       1    1 ; ...
       0    0 ;];
   
inp_transition_list = ['00-01'; '01-00'; '00-10'; '10-00'; ...
                       '00-11'; '11-01'; '01-10'; '10,11'; ...
                       '11-10'; '10-01'; '01-11'; '11-00'];

% 0 => no transition +1 => pos transition -1 => neg transition                  
xor_transition = [1, -1, 1, -1, 0, 1, 0, -1, 1, 0, -1, 0];
xnor_transition = -xor_transition;

%- Delay calculations -
delay = zeros(N,3);
inp_sig = [evalsig(transientsim, 'a'), evalsig(transientsim, 'b')] - globals.supply/2;
out_sig = [evalsig(transientsim, 'c1'), evalsig(transientsim, 'c2'), evalsig(transientsim, 'c3') ]- globals.supply/2;
for i=1:N
    begintime = (i)*(T - 0.2e-9);
    bidx = getElementNumber(time,begintime);
    endtime = (i+1)*(T - 0.2e-9);
    eidx = getElementNumber(time,endtime);
    
    tr_type = inp(i+1,:)-inp(i,:);
    if(tr_type(1) == 1)
      tra = findPositiveZeroCrossings(time(bidx:eidx),inp_sig(bidx:eidx,1));
    elseif(tr_type(1) == -1)
      tra = findNegativeZeroCrossings(time(bidx:eidx),inp_sig(bidx:eidx,1));
    else
      tra =Inf;
    end   
    if(tr_type(2) == 1)
      trb = findPositiveZeroCrossings(time(bidx:eidx),inp_sig(bidx:eidx,2));
    elseif(tr_type(2) == -1)
      trb = findNegativeZeroCrossings(time(bidx:eidx),inp_sig(bidx:eidx,2));
    else
      trb = Inf;
    end   
    inp_tr_time = min(tra,trb);
        
    
    if(xor_transition(i) == 1)
        delay(i,1) = findPositiveZeroCrossings(time(bidx:eidx),out_sig(bidx:eidx,1)) - inp_tr_time;
        delay(i,2) = findPositiveZeroCrossings(time(bidx:eidx),out_sig(bidx:eidx,2)) - inp_tr_time;
        delay(i,3) = findNegativeZeroCrossings(time(bidx:eidx),out_sig(bidx:eidx,3)) - inp_tr_time;
    elseif(xor_transition(i) == -1)
        delay(i,1) = findNegativeZeroCrossings(time(bidx:eidx),out_sig(bidx:eidx,1)) - inp_tr_time;
        delay(i,2) = findNegativeZeroCrossings(time(bidx:eidx),out_sig(bidx:eidx,2)) - inp_tr_time;
        delay(i,3) = findPositiveZeroCrossings(time(bidx:eidx),out_sig(bidx:eidx,3)) - inp_tr_time;
    elseif(xor_transition(i) == 0)
      delay(i,:) = 0; 
    end
    
end
disp('Delay (ps)');
for i=1:N
    fprintf('%s: ',string(inp_transition_list(i,:)));
    
    for j=1:3
    fprintf(' %03.1f ',delay(i,j)*1e12);     
    end
    fprintf('\n')
end

disp(['Max Delay through  (ps)']);
for j=1:3
    fprintf(' %f ',max(delay(:,j))*1e12);     
end
disp(' ')
%- Power calculations -
%----------------------
I_vdd1   = evalsig(transientsim, 'I_vdd1');
I_vdd2   = evalsig(transientsim, 'I_vdd2');
I_vdd3   = evalsig(transientsim, 'I_vdd3');
N1=15;
Charge = zeros(N1,3);
for i = 1:N1
    
    begintime = max((4e-9*(i-1)-0.2e-9) , 0);
    endtime = min((4e-9*i-0.2e-9), 60e-9);
    
    if begintime == 0
        beginindex = 1;
    else
        beginindex = getElementNumber(time,begintime);
    end
    endindex = getElementNumber(time,endtime);
    
    Charge(i,1) = trapz(time(beginindex:endindex),I_vdd1(beginindex:endindex));
    Charge(i,2) = trapz(time(beginindex:endindex),I_vdd2(beginindex:endindex));
    Charge(i,3) = trapz(time(beginindex:endindex),I_vdd3(beginindex:endindex));
end
vdd = globals.supply;
Energy = -Charge*vdd;
disp(['Switching Energy Per event X Per Design  (fJ)'])
SwitchingEnergy = Energy*1e15; % energy of the switching event
for i=1:N
    fprintf('%s: ',string(inp_transition_list(i,:)));
    for j=1:3
    fprintf(' %f ',SwitchingEnergy(i,j));     
    end
    fprintf('\n')
end
disp(['Average Switching Energy Per Design  (fJ)']);
for j=1:3
    fprintf(' %f ',mean(abs(SwitchingEnergy(:,j))));     
end
disp(' ')
disp(' ')
DCpowerWC = Energy(1,:)/T; % power = energy/time
disp(['Worse case DC power XORBase: ',num2str(DCpowerWC(1)*1e9),' nW']);
disp(['Worse case DC power MYXOR: ',num2str(DCpowerWC(2)*1e9),' nW']);
disp(['Worse case DC power MYXNOR: ',num2str(DCpowerWC(3)*1e9),' nW']);


