# DDIS_Design_Dandekar_Mohit

-- The matlab script included is to demonstrate the 
   additional tests mentioned in the report. (requires the modified testvec)

-- On top there are two variables : 'analysis' and 'animate'
   -- To enable the sweep of carry at bit i experient set 'analysis=1'
   -- To view the switching activity set 'animate=1' (only relevant if analysis==1)
   


-- the m2sfile can be run with the standard matlab script, provided a global
   variable 'globals.sim_time' is set to '16e-9' otherwise the variable 
   can be replaced in the m2s file by '16ns'

   -- this was done to control the transient sim time from matlab


-- TO RUN the sweep from generate overview, disable the 'clear all' at the top of the file 
