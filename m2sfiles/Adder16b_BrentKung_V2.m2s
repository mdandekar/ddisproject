******************************
**** 16b Brent-Kung adder ****
******************************
.param supply = $globals.supply
.param halfsupply = $globals.supply/2
.param gamma =$globals.gamma

* Some simulation options
*-------------------------
.options post nomod 
.option opts fast parhier=local

.lib '$< strcat(pwd, '/Resources/Technology/tech_wrapper.lib') >$ ' tt
.tran 0.005n 48n
.vec '$< strcat(pwd, '/m2sfiles/Adder16b.vec')>$'

.probe i

Vdd vdd vss supply
Vdd2 vdd2 vss supply
Vss vss 0 0 

* Actual circuit
*----------------
$ n = 16;
$ for j = 0:n-1
    xNOTa$j  a$j  aN$j     vdd2 vss MYNOT
    xNOTaN$j aN$j a_buff$j vdd2 vss MYNOT
    xNOTb$j  b$j  bN$j     vdd2 vss MYNOT
    xNOTbN$j bN$j b_buff$j vdd2 vss MYNOT
$ end


Xadder $xbus('a_buff',0:15) $xbus('b_buff',0:15) $xbus('s',0:16) vdd vss ADDER

   
$ for i = 1:16
    xNOT$i  s$i sN$i vdd2 vss MYNOT multfac = 16
$ end



* Brent-Kung Adder subcircuit
*-----------------------------
.SUBCKT ADDER a0 a1 a2 a3 a4 a5 a6 a7 a8 a9 a10 a11 a12 a13 a14 a15 b0 b1 b2 b3 b4 b5 b6 b7 b8 b9 b10 b11 b12 b13 b14 b15 s0 s1 s2 s3 s4 s5 s6 s7 s8 s9 s10 s11 s12 s13 s14 s15 s16 vdd vss

  *------ Layer 1 ------
  xGPBARNode1x0  a0  b0  genb0  propb0  propbs0  vdd vss GPBAR gatesize='1' 
  xGPBARNode1x1  a1  b1  genb1  propb1  propbs1  vdd vss GPBAR gatesize='1' 
  xGPBARNode1x2  a2  b2  genb2  propb2  propbs2  vdd vss GPBAR gatesize='1' 
  xGPBARNode1x3  a3  b3  genb3  propb3  propbs3  vdd vss GPBAR gatesize='1' 
  xGPBARNode1x4  a4  b4  genb4  propb4  propbs4  vdd vss GPBAR gatesize='1' 
  xGPBARNode1x5  a5  b5  genb5  propb5  propbs5  vdd vss GPBAR gatesize='1' 
  xGPBARNode1x6  a6  b6  genb6  propb6  propbs6  vdd vss GPBAR gatesize='1' 
  xGPBARNode1x7  a7  b7  genb7  propb7  propbs7  vdd vss GPBAR gatesize='1' 
  xGPBARNode1x8  a8  b8  genb8  propb8  propbs8  vdd vss GPBAR gatesize='1' 
  xGPBARNode1x9  a9  b9  genb9  propb9  propbs9  vdd vss GPBAR gatesize='1' 
  xGPBARNode1x10 a10 b10 genb10 propb10 propbs10 vdd vss GPBAR gatesize='1' 
  xGPBARNode1x11 a11 b11 genb11 propb11 propbs11 vdd vss GPBAR gatesize='1' 
  xGPBARNode1x12 a12 b12 genb12 propb12 propbs12 vdd vss GPBAR gatesize='1' 
  xGPBARNode1x13 a13 b13 genb13 propb13 propbs13 vdd vss GPBAR gatesize='1' 
  xGPBARNode1x14 a14 b14 genb14 propb14 propbs14 vdd vss GPBAR gatesize='1' 
  xGPBARNode1x15 a15 b15 genb15 propb15 propbs15 vdd vss GPBAR gatesize='1' 

  *------ Layer 2 ------
  xDBUFFERNode2x0 propb0  genb0  propb0xbuf0  genb0xbuf0  vdd vss DBUFFER gate_size='1'
  xDOTBARNode2x0 genb0   prob0  genb1  propb1  genb1_0  propb1_0 vdd vss DOTBAR multfac='1'

  xDBUFFERNode2x1 propb2  genb2  propb2xbuf0  genb2xbuf0  vdd vss DBUFFER gate_size='1'
  xDOTBARNode2x1 genb2  propb2  genb3  propb3  genb3_2   propb3_2   vdd vss DOTBAR multfac='1'

  xDBUFFERNode2x2 propb4  genb4  propb4xbuf0  genb4xbuf0  vdd vss DBUFFER gate_size='1'
  xDOTBARNode2x2 genb4  propb4  genb5  propb5  genb5_4   propb5_4   vdd vss DOTBAR multfac='1'

  xDBUFFERNode2x3 propb6  genb6  propb6xbuf0  genb6xbuf0  vdd vss DBUFFER gate_size='1'
  xDOTBARNode2x3 genb6  propb6  genb7  propb7  genb7_6   propb7_6   vdd vss DOTBAR multfac='1'

  xDBUFFERNode2x4 propb8  genb8  propb8xbuf0  genb8xbuf0  vdd vss DBUFFER gate_size='1'
  xDOTBARNode2x4 genb8  propb8  genb9  propb9  genb9_8   propb9_8   vdd vss DOTBAR multfac='1'

  xDBUFFERNode2x5 propb10 genb10 propb10xbuf0 genb10xbuf0 vdd vss DBUFFER gate_size='1'
  xDOTBARNode2x5 genb10 propb10 genb11 propb11 genb11_10 propb11_10 vdd vss DOTBAR multfac='1'
 
  xDBUFFERNode2x6 propb12 genb12 propb12xbuf0 genb12xbuf0 vdd vss DBUFFER gate_size='1'
  xDOTBARNode2x6 genb12 propb12 genb13 propb13 genb13_12 propb13_12 vdd vss DOTBAR multfac='1'

  xDBUFFERNode2x7 propb14 genb14 propb14xbuf0 genb14xbuf0 vdd vss DBUFFER gate_size='1'
  xDOTBARNode2x7 genb14 propb14 genb15 propb15 genb15_14 propb15_14 vdd vss DOTBAR multfac='1'


  *------ Layer 3 ------
  xDBUFFERNode3x0 propb1_0   genb1_0   propb1_0xbuf0   genb1_0xbuf0   vdd vss DBUFFER gate_size='1'
  xDOTBARNode3x0 genb1_0   propb1_0   genb3_2   propb3_2   genb3_0   propb3_0   vdd vss DOTBAR multfac='1'

  xDBUFFERNode3x1 propb5_4   genb5_4   propb5_4xbuf0   genb5_4xbuf0   vdd vss DBUFFER gate_size='1'
  xDOTBARNode3x1 genb5_4   propb5_4   genb7_6   propb7_6   genb7_4   propb7_4   vdd vss DOTBAR multfac='1'

  xDBUFFERNode3x2 propb9_8   genb9_8   propb9_8xbuf0   genb9_8xbuf0   vdd vss DBUFFER gate_size='1'
  xDOTBARNode3x2 genb9_8   propb9_8   genb11_10 propb11_10 genb11_8  propb11_8  vdd vss DOTBAR multfac='1'

  xDBUFFERNode3x3 propb13_12 genb13_12 propb13_12xbuf0 genb13_12xbuf0 vdd vss DBUFFER gate_size='1'
  xDOTBARNode3x3 genb13_12 propb13_12 genb15_14 propb15_14 genb15_12 propb15_12 vdd vss DOTBAR multfac='1'

  
  *------ Layer 4 ------
  xDBUFFERNode4x0 propb3_0  genb3_0  propb3_0xbuf0  genb3_0xbuf0 vdd vss DBUFFER gate_size='1'
  xDOTBARNode4x0 genb3_0  propb3_0  genb7_4   propb7_4   genb7_0  propb7_0 vdd vss DOTBAR multfac='1' 

  xDBUFFERNode4x1 propb11_8 genb11_8 propb11_8xbuf0 genb11_8xbuf0 vdd vss DBUFFER gate_size='1'
  xDOTBARNode4x1 genb11_8 propb11_8 genb15_12 propb15_12 genb15_8 propb15_8 vdd vss DOTBAR multfac='1'


  *------ Layer 5 ------
  xDBUFFERNode5x0 propb3_0xbuf0 genb3_0xbuf0 prob3_0xbuf1 genb3_0xbuf1 vdd vss DBUFFER gate_size='1'
  xDOTBARNode5x0 genb3_0xbuf0 propb3_0xbuf0 genb5_4xbuf0 propb5_4xbuf0  genb5_0 propb5_0 vdd vss DOTBAR multfac='1'

  xDBUFFERNode5x1 propb7_0 genb7_0 prob7_0xbuf0 genb7_0xbuf0 vdd vss DBUFFER gate_size='1'
  xDOTBARNode5x1 genb7_0 propb7_0 genb11_8xbuf0 propb11_8xbuf0 genb11_0 propb11_0 vdd vss DOTBAR multfac='1'

  *------ Layer 6 ------  
  xDBUFFERNode6x0 propb7_0xbuf0 genb7_0xbuf0 prob7_0xbuf1 genb7_0xbuf1 vdd vss DBUFFER gate_size='1'
  xDOTBARNode6x0 genb7_0xbuf0 propb7_0xbuf0 genb9_8xbuf0 propb9_8xbuf0  genb9_0 propb9_0 vdd vss DOTBAR multfac='1'

  xDBUFFERNode6x1 propb11_0 genb11_0 prob11_0xbuf0 genb11_0xbuf0 vdd vss DBUFFER gate_size='1'
  xDOTBARNode6x1 genb11_0 propb11_0 genb13_12xbuf0 propb13_12xbuf0 genb13_0 propb13_0 vdd vss DOTBAR multfac='1'  
 
  *------ Layer 7 ------  
  xDOTBARNode7_0 genb1_0xbuf0 propb1_0xbuf0 genb2xbuf0 propb2xbuf0 genb2_0 propb2_0 vdd vss DOTBAR multfac='1'  

  xDOTBARNode7_1 genb3_0xbuf1 propb3_0xbuf1 genb4xbuf0 propb4xbuf0 genb4_0 propb4_0 vdd vss DOTBAR multfac='1'  

  xDOTBARNode7_2 genb5_0 propb5_0 genb6xbuf0 propb6xbuf0 genb6_0 propb6_0 vdd vss DOTBAR multfac='1'  

  xDOTBARNode7_3 genb7_0xbuf1 propb7_0xbuf1 genb8xbuf0 propb8xbuf0 genb8_0 propb8_0 vdd vss DOTBAR multfac='1'  

  xDOTBARNode7_4 genb9_0 propb9_0 genb10xbuf0 propb10xbuf0 genb10_0 propb10_0 vdd vss DOTBAR multfac='1'  

  xDOTBARNode7_5 genb11_0xbuf0 propb11_0xbuf0 genb12xbuf0 propb12xbuf0 genb12_0 propb12_0 vdd vss DOTBAR multfac='1'  

  xDOTBARNode7_6 genb13_0 propb13_0 genb14xbuf0 propb14xbuf0 genb14_0 propb14_0 vdd vss DOTBAR multfac='1'  

  xDOTBARNode7_7 genb7_0xbuf1 propb7_0xbuf1 genb15_8 propb15_8 genb15_0 propb15_0 vdd vss DOTBAR multfac='1'  

  xDBUFFERNode7x8 propb7_0xbuf1 genb7_0xbuf1 prob7_0xbuf2 genb7_0xbuf2 vdd vss DBUFFER gate_size='1'
  

  *------ Layer 8 ------  
  xXOR_0 propbs0   vdd           s0 vdd vss MYXOR
  xXOR_1 propbs1   genb0xbuf0    s1 vdd vss MYXOR
  xXOR_2 propbs2   genb1_0xbuf0  s2 vdd vss MYXOR 
  xXOR_3 propbs3   genb2_0       s3 vdd vss MYXOR 
  xXOR_4 propbs4   genb3_0xbuf1  s4 vdd vss MYXOR 
  xXOR_5 propbs5   genb4_0       s5 vdd vss MYXOR 
  xXOR_6 propbs6   genb5_0       s6 vdd vss MYXOR 
  xXOR_7 propbs7   genb6_0       s7 vdd vss MYXOR 
  xXOR_8 propbs8   genb7_0xbuf2  s8 vdd vss MYXOR 
  xXOR_9 propbs9   genb8_0       s9 vdd vss MYXOR 
  xXOR_10 propbs10 genb9_0       s10 vdd vss MYXOR 
  xXOR_11 propbs11 genb10_0      s11 vdd vss MYXOR 
  xXOR_12 propbs12 genb11_0xbuf0 s12 vdd vss MYXOR 
  xXOR_13 propbs13 genb12_0      s13 vdd vss MYXOR 
  xXOR_14 propbs14 genb13_0      s14 vdd vss MYXOR 
  xXOR_15 propbs15 genb14_0      s15 vdd vss MYXOR 
  xXOR_16 vdd     genb15_0      s16 vdd vss MYXOR 
.ENDS ADDER




* Other subcircuits

*-------------------
.SUBCKT MYNAND inp_a inp_b out_c vdd vss multfac='1'

  xM1 n1 inp_a vss vss MOSN w='multfac*2*120e-9' l='45e-9'
  xM2 out_c inp_b n1 vss MOSN w='multfac*2*120e-9' l='45e-9'

  xM3 out_c inp_a vdd vdd MOSP w='multfac*gamma*120e-9' l='45e-9' 
  xM4 out_c inp_b vdd vdd MOSP w='multfac*gamma*120e-9' l='45e-9' 

.ENDS MYNAND


*-------------------
.SUBCKT MYNOR inp_a inp_b out_c vdd vss multfac='1'
  
      xM1 out_c inp_a vss vss MOSN w='multfac*120e-9' l='45e-9'
      xM2 out_c inp_b vss vss MOSN w='multfac*120e-9' l='45e-9'

      xM3 n1 inp_a vdd vdd MOSP w='multfac*gamma*2*120e-9' l='45e-9'
      xM4 out_c inp_b n1 vdd MOSP w='multfac*gamma*2*120e-9' l='45e-9'

.ENDS MYNOR



*-------------
.SUBCKT MYXNOR inp_a inp_b out_c vdd vss multfac='1'

     xM1 n12 inp_a vss vss MOSN w='multfac*1*2*120e-9' l='45e-9'
     xM2 n12 inp_a vdd vdd MOSP w='multfac*2*gamma*120e-9' l='45e-9'

     xM3 n34 inp_b vss vss MOSN w='multfac*1*2*120e-9' l='45e-9'
     xM4 n34 inp_b vdd vdd MOSP w='multfac*2*gamma*120e-9' l='45e-9'

     xM5 out_c n34 n12 vss MOSN w='multfac*1*2*120e-9' l='45e-9'
     xM6 out_c n12 n34 vss MOSN w='multfac*1*2*120e-9' l='45e-9'

     xM7 out_c n34 n78 vdd MOSP w='multfac*2*gamma*120e-9' l='45e-9'
     xM8 n78 n12 vdd vdd MOSP w='multfac*2*gamma*120e-9' l='45e-9'

     xM9 out_c inp_b n910 vdd MOSP w='multfac*1*gamma*120e-9' l='45e-9'
     xM10 n910 inp_a vdd vdd MOSP w='multfac*1*gamma*120e-9' l='45e-9'

.ENDS MYXNOR


*-------------
.SUBCKT MYXOR inp_a inp_b out_c vdd vss multfac='1'

     xM1 n12 inp_a vdd vdd MOSP w='multfac*2*gamma*120e-9' l='45e-9'
     xM2 n12 inp_a vss vss MOSN w='multfac*2*1*120e-9' l='45e-9'

     xM3 n34 inp_b vdd vdd MOSP w='multfac*2*gamma*120e-9' l='45e-9'
     xM4 n34 inp_b vss vss MOSN w='multfac*2*1*120e-9' l='45e-9'

     xM5 out_c n34 n12 vdd MOSP w='multfac*2*gamma*120e-9' l='45e-9'
     xM6 out_c n12 n34 vdd MOSP w='multfac*2*gamma*120e-9' l='45e-9'

     xM7 out_c n34 n78 vss MOSN w='multfac*2*1*120e-9' l='45e-9'
     xM8 n78 n12 vss vss MOSN w='multfac*2*1*120e-9' l='45e-9'

     xM9 out_c inp_b n910 vss MOSN w='multfac*1*1*120e-9' l='45e-9'
     xM10 n910 inp_a vss vss MOSN w='multfac*1*1*120e-9' l='45e-9'

.ENDS MYXOR



.SUBCKT GPBAR A B Genb Propb Propbs vdd vss gatesize='1'
    xNAND A B Genb  vdd vss MYNAND multfac='gatesize*1'
    xNOR A B Propb vdd vss MYNOR multfac='gatesize*1'
    xXNOR A B Propbs vdd vss MYXNOR multfac='gatesize*1'
.ENDS GPBAR


.SUBCKT DOTBAR inp_glb inp_plb inp_gub inp_pub out_gulb out_pulb vdd vss multfac='1'

    xM1 n0 inp_gub vss vss MOSN w='multfac*2*120e-9' l='45e-9'

    xM2 n10 inp_glb n0 vss MOSN w='multfac*2*120e-9' l='45e-9'
    xM3 n11 inp_pub n0 vss MOSN w='multfac*2*120e-9' l='45e-9'

    xM4 n10 inp_glb vdd vdd MOSP w='multfac*gamma*120e-9' l='45e-9'

    xM5 n10 inp_gub vdd vdd MOSP w='multfac*gamma*120e-9' l='45e-9'
    xM6 n11 inp_gub vdd vdd MOSP w='multfac*gamma*120e-9' l='45e-9'

    xM7 n11 inp_pub vdd vdd MOSP w='multfac*gamma*120e-9' l='45e-9'


    xM8 n2 n11 vss vss MOSN w='multfac*2*120e-9' l='45e-9'
    xM9 out_gulb n10 n2 vss MOSN w='multfac*2*120e-9' l='45e-9'

    xM10 out_gulb n10 vdd vdd MOSP w='multfac*gamma*120e-9' l='45e-9'
    xM11 out_gulb n11 vdd vdd MOSP w='multfac*gamma*120e-9' l='45e-9'


    xM12 pul inp_pub vss vss MOSN w='multfac*1*120e-9' l='45e-9'
    xM13 pul inp_plb vss vss MOSN w='multfac*1*120e-9' l='45e-9'

    xM14 pul inp_pub n3 vdd MOSP w='multfac*2*gamma*120e-9' l='45e-9'
    xM15 n3 inp_plb vdd vdd MOSP w='multfac*2*gamma*120e-9' l='45e-9'

    xM16 out_pulb pul vss vss MOSN w='multfac*1*120e-9' l='45e-9'
    xM17 out_pulb pul vdd vdd MOSP w='multfac*gamma*120e-9' l='45e-9'

.ENDS DOTBAR


.SUBCKT DBUFFER input1 input2 output1 output2 vdd vss gate_size='1'
  xbuf1 input1 output1 vdd vss BUFFER gatesize='gate_size*1'
  xbuf2 input2 output2 vdd vss BUFFER gatesize='gate_size*1'
.ENDS DBUFFER


.SUBCKT BUFFER input output vdd vss gatesize='1'
  xinv1 input n0  vdd vss MYNOT multfac='gatesize*1'
  xinv2 n0 output vdd vss MYNOT multfac='gatesize*1'
.ENDS BUFFER


.SUBCKT MYNOT input output vdd vss multfac='1'
    xM1 output input vss vss MOSN w='multfac*120e-9' l='45e-9'
    xM2 output input vdd vdd MOSP w='multfac*gamma*120e-9' l='45e-9'
.ENDS MYNOT




.END

