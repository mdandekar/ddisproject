close all
clear all
clear transientsim
clear acsim
inputfile = 'myDOTOP';

globals.load_size = 4;
globals.supply = 1;
globals.gate_size = 1;
globals.gamma = 2;


runSpice;
    

time        = evalsig(transientsim, 'TIME');
N = 12; % amount of patterns simulated
T = 4e-9; % period of pattern


