







%y1 = [0.01:0.01:2];
%y2 = [0.01:0.01:2];
%y3 = [0.01:0.01:2];

g = 1.5;




syms y1 y2 y3

f1 = (y2+1)/y2 + g;

f2 = (y2+1) + (y1+1)*g + 1 + (y3+1)*g;

f3 = (y2+1) + ((y1+1)/y1)*g;

f4 = 1 + ((y3+1)/y3)*g;

varsdot = [y1 y2 y3];
eqndot = [(f2-f1)==0, (f3-f1)==0, (f4-f1)==0];
assume(y1, 'real');
assume(y2, 'real');
assume(y3, 'real');
[soln_y1, soln_y2 ,soln_y3] = vpasolve(eqndot,varsdot,[0 Inf; 0 Inf; 0 Inf]);
fprintf('Sizing of DOT op [y1 y2 y3] = [%0.1f  %0.1f %0.1f]\n',soln_y1, soln_y2, soln_y3);

fprintf('LE on input gub = %2.2f/(%2f)\n',(soln_y2+1)/soln_y2 + g,(1+g));
fprintf('LE on input pub = %2.2f/(%2f)\n',(soln_y2+1) + (soln_y1+1)*g + 1 + (soln_y3+1)*g,(1+g));
fprintf('LE on input glb = %2.2f/(%2f)\n',(soln_y2+1) + ((soln_y1+1)/soln_y1)*g,(1+g));
fprintf('LE on input plb = %2.2f/(%2f)\n',1 + ((soln_y3+1)/soln_y3)*g,(1+g));
disp('')


syms x1 x2 x3

h1 = 1 + g*(x2+1)/x2;

h2 = (x1+1) + g*(x2+1) + (x3+1) + g;

h3 = (x1+1)/x1 + g*(x2+1);

h4 = (x3+1)/x3  + g;

varsdotbar = [x1 x2 x3];
eqndotbar = [(h2-h1)==0, (h3-h1)==0, (h4-h1)==0];
assume(x1, 'real');
assume(x2, 'real');
assume(x3, 'real');
[soln_x1 ,soln_x2, soln_x3] = vpasolve(eqndotbar,varsdotbar,[0 Inf; 0 Inf; 0 Inf]);
fprintf('Sizing of DOTBAR op [x1 x2 x3] = [%0.1f  %0.1f %0.1f]\n',soln_x1, soln_x2, soln_x3);

fprintf('LE on input gu = %2.2f/(%2f)\n',1 + g*(soln_x2+1)/soln_x2,(1+g));
fprintf('LE on input pu = %2.2f/(%2f)\n',(soln_x1+1) + g*(soln_x2+1) + (soln_x3+1) + g,(1+g));
fprintf('LE on input gl = %2.2f/(%2f)\n',(soln_x1+1)/soln_x1 + g*(soln_x2+1),(1+g));
fprintf('LE on input pl = %2.2f/(%2f)\n',(soln_x3+1)/soln_x3  + g,(1+g));


syms x1 x2 x3 y1 y2 y3 gv

f1 = (y2+1)/y2 + gv;

f2 = (y2+1) + (y1+1)*gv + 1 + (y3+1)*gv;

f3 = (y2+1) + ((y1+1)/y1)*gv;

f4 = 1 + ((y3+1)/y3)*gv;

h1 = 1 + gv*(x2+1)/x2;

h2 = (x1+1) + gv*(x2+1) + (x3+1) + gv;

h3 = (x1+1)/x1 + gv*(x2+1);

h4 = (x3+1)/x3  + gv;
assume(gv,'real')
assume(y1, 'real');
assume(y2, 'real');
assume(y3, 'real');
assume(x1, 'real');
assume(x2, 'real');
assume(x3, 'real');
[soln_x1,soln_x2,soln_x3,soln_y1,soln_y2,soln_y3,sg]=vpasolve([(f2-f1)==0,(f3-f1)==0,(f4-f1)==0,(h2-f1)==0,(h3-f1)==0,(h4-f1)==0,(h1-f1)==0],[x1 x2 x3 y1 y2 y3 gv],[0 Inf;0 Inf;0 Inf;0 Inf;0 Inf;0 Inf;0 Inf]);

fprintf('Sizing of DOT op [y1 y2 y3] = [%0.1f  %0.1f %0.1f]\n',soln_y1, soln_y2, soln_y3);

fprintf('LE on input gub = %2.2f/(%2f)\n',(soln_y2+1)/soln_y2 + sg,(1+sg));
fprintf('LE on input pub = %2.2f/(%2f)\n',(soln_y2+1) + (soln_y1+1)*sg + 1 + (soln_y3+1)*sg,(1+sg));
fprintf('LE on input glb = %2.2f/(%2f)\n',(soln_y2+1) + ((soln_y1+1)/soln_y1)*sg,(1+sg));
fprintf('LE on input plb = %2.2f/(%2f)\n',1 + ((soln_y3+1)/soln_y3)*sg,(1+sg));
disp('')

fprintf('Sizing of DOTBAR op [x1 x2 x3] = [%0.1f  %0.1f %0.1f]\n',soln_x1, soln_x2, soln_x3);

fprintf('LE on input gu = %2.2f/(%2f)\n',1 + sg*(soln_x2+1)/soln_x2,(1+sg));
fprintf('LE on input pu = %2.2f/(%2f)\n',(soln_x1+1) + sg*(soln_x2+1) + (soln_x3+1) + sg,(1+sg));
fprintf('LE on input gl = %2.2f/(%2f)\n',(soln_x1+1)/soln_x1 + sg*(soln_x2+1),(1+sg));
fprintf('LE on input pl = %2.2f/(%2f)\n',(soln_x3+1)/soln_x3  + sg,(1+sg));
