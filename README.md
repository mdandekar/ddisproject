
### What is this repository for? ###

Development files for transistor level design of 16 bit Brent-Kung topology adder in 45nm HP CMOS done towards class project in course on digital systems at KU Leuven. 

### How do I get set up? ###

The design framework uses mat2spice; a MATLAB wrapper to SPICE (hspice) that provides some design automation within MATLAB environment primarily
for writing marked up hspice circuit description but more importantly for analysis of simulation data.

seperate hspice installation is required to run and some files in the utilities must be modified to point to the hspice executable in the users system. 

### Contribution guidelines ###

This repository is meant to hold the design files and can be viewed by anyone, but be advised that copying the design topologies and transistor  sizing from this project and using it in the design project in subsequent years would be considred pagiarism and the university may object to it. 
