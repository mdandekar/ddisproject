%% LyX 2.0.2 created this file.  For more info, see http://www.lyx.org/.
%% Do not edit unless you really know what you are doing.
\documentclass[english]{article}
\usepackage[OT1]{fontenc}
\usepackage[latin9]{inputenc}
\usepackage{geometry}
\geometry{verbose,tmargin=2cm,bmargin=2cm,lmargin=2cm,rmargin=2cm}
\usepackage{float}
\usepackage{graphicx}
\usepackage{babel}

\usepackage{tikz}
\usepackage{pgfplots}
\usepackage{circuitikz}
\usetikzlibrary{arrows, positioning, calc,shapes}
\usepackage{pdfpages}
\usepackage{epstopdf}
\usepackage{amsmath}

\begin{document}

\title{Design of digital integrated systems: optimisation of a 16 bit Brent-Kung
adder}


\author{Mohit Dandekar}

\maketitle

\section{Design and Optimization}

The optimization of the baseline 16 bit Brent-Kung adder is required to minimize both leakage power and the switching energy subject to the maximum delay of 650 ps. The original design used the propagate generate equations in non-inverted form which results into poor stage delay leading to overall delay of ~950 ps. The first optimization step looks into transformation of the PG equations. The radix 2 PG equations combines propagate generate signals from he upper and lower nodes ($p_u$, $g_u$, $p_l$, $g_l$) to form a joint propagate-generate signals ($p_{ul}$, $g_{ul}$) 

\begin{align*}
   g_{ul} &= g_u + p_u \cdot g_l\\
   p_{ul} &= p_u \cdot p_l
\end{align*}
As these equations are non inverting, they pose a problem in static CMOS implementation. Hence the following transformation is used and two new operator are defined based on transformations to the original equations using DeMorgan's laws.

\begin{table}[h!]
\centering
\begin{tabular}{c|c}
\hline
DOT Operator & DOTBAR operator\\
\hline
\\
$\begin{aligned}
   g_{ul} &= \overline{\overline{g_u}  \cdot \left(\overline{p_u} + \overline{g_l}\right)}\\
   p_{ul} &= \overline{\overline{p_u} +  \overline{p_l}}
\end{aligned}$
&
$\begin{aligned}
   \overline{g_{ul}} &= \overline{g_u + p_u \cdot g_l}\\
   \overline{p_{ul}} &= \overline{p_u \cdot p_l}
\end{aligned}$\\
\hline
\end{tabular}
\end{table}
Along the same line the initial bitwise propagate generate is also implemented in inverted form. Hence with the inverted propagate generate signals available, the adder topology is transformed to use alternating version of DOT/DOTBAR as shown in figure \ref{adderschem}. In order to maintain the inversion along the stages inverting buffers need to be added to ensure the correct polarity of the inputs wherever require. Apart from only meeting the polarity, the inverting buffers also serve the purpose of making the load seen by almost all non terminal dot-operators to be equal. This regularizes the loading of the DOT operator as seen for last node on bit 7. \\

\begin{figure}[h]\label{arch}
\centering
\includegraphics[scale=0.4]{arch.png}
\caption{Architecture for the Adder}
\end{figure}

The second optimization on architecture level is the logic of computing the final sum. The sum at each bit location $S_i$ is given as $S_i = G_{(i-1):0} \oplus a_i \oplus b_i$. This operation has two constituent elements, first the input XOR $a_i \oplus b_i$ that can be all computed in parallel and the carry in at bit i $G_{i-1}:0$ which the Brent-Kung tree computes efficiently, albeit with limit on the parallelization. This implies that the generation of the input XOR can be done beforehand in parallel till the carry generation propagates through the Brent-Kung tree and then the sum is generated via final XOR with the incoming carry. A better way is to pre-compute both XOR/XNOR of the inputs and then MUX the 'correct' value based on the incoming carry. This latter technique has an advantage that the delay of the MUX could be made a bit smaller than a full XOR, and the effort seen at the carry in input driving a 2X1 MUX selectline is lower than input of an XOR gate which means a MUX would load the PG tree less and will yield delay benefit.
The resulting architecture is captured in figure \ref{arch}. \\

The optimization of the circuit blocks requires transistor level design for each logic function. The schematic of which is captured in figure \ref{cktblocks}. The DOT/DOTBAR operators have been designed for both delay and energy as the carry generation is the limiting element in the architecture, but the input XOR/XNOR can be tuned for low energy as the delay of the XOR/XNOR block has to be just a bit lesser than the worse case carry delay. The MUX design requires not only the energy and delay considerations but also the inclusion of the driving stage to drive the given load. The XOR/XNOR design computes both output jointly. The design is inspired from the designs proposed by Wang et. al. in JSSC 1994 and a design by Goel, Elgamel et. al 2006 IEEE. The modifications to the original design is the addition of input buffers to reduce the load on the input bits as they also drive the PG network. \\

The optimization of the DOT/DOTBAR operator has been done via simulation. The first set of sizing was done by maintaining unit drive and sizing the transistors accordingly. However it is seen via experimentation that this is not optimal. The activity on each input is not the same for the test case and hence a reduced drive strength has been used in some branches of the DOT/DOTBAR circuits.  Figure \ref{cktblocks} captures the transistor sizing details. Also as this is a 45 nm node the NMOS/PMOS relative sizing is not required to be 1:2. Instead the $\gamma$ parameter for PMOS relative size is taken to be 1 and results into decrease in both energy and delay. 



\section{Results}


The testcase for the worst delay is a = 0x0001 and b=0x7fff resulting into the longest carry propagate through the tree. However this may not be the slowest path post sizing hence a full simulation of carry propagation to all bits has been carried out iteratively. The figure at the bottom of table \ref{my-label} shows the delays with inputs to target carry propogation to bit i. Note that the graph also shows the contribution to the delay by each block, i.e. XOR/XNOR the PG network and the final MUX. \\

The results in table \ref{table1} have been achieved using the 45nm LP technology. For the given target delay the use of HP devices do not yield benefit as the leakage energy scales significantly. 



\begin{table}[h!]
\centering
\begin{tabular}{ |l|r|r| }
\hline
    & 45nm\_LP  & 45nm\_HP\\
\hline
Delay & 650 ps  & 640 ps\\
Supply & 0.88564 V & 0.56 V\\
Switching Energy & 133.6756 fJ & 46.38 fJ\\
DC power & 1.7116 nW  & 54.38 nW\\ 
\hline
\end{tabular}
\caption{Final performance of the circuit}
\label{table1}
\end{table}


\begin{table}[hb!]
\centering
\label{my-label}
\begin{tabular}{cc}
\includegraphics[scale=0.4]{energydelay.pdf} & \includegraphics[scale=0.4]{supplydelay.pdf}\\
\includegraphics[scale=0.4]{supplyleakage.pdf} & \includegraphics[scale=0.4]{supplyenergy.pdf}\\
\multicolumn{2}{c}{Results for supply voltage sweep with 45nmLP}\\
\multicolumn{2}{c}{\includegraphics[scale=0.4]{pathdelays.png}}
\end{tabular}
\end{table}



\pagebreak

\begin{figure}[h!]\label{adderschem}
 \centering 
 \includegraphics[scale=0.6]{adder.png}
 \caption{Schematic of the optimised adder}
\end{figure}

\pagebreak

\begin{figure}[h!]\label{cktblocks}
 \centering 
 \includegraphics[scale=0.6]{cktblock.png}
 \caption{Schematic of the Circuit Blocks}
\end{figure}



\end{document}


