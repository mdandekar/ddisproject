close all
clear all
clear transientsim
clear acsim
inputfile = 'myNANDNOR';

globals.load_size = 1;
globals.supply = 1;
globals.gate_size = 1;
globals.gamma = 2;


runSpice;
    

time        = evalsig(transientsim, 'TIME');
N = 12; % amount of patterns simulated
T = 4e-9; % period of pattern

plotsig(transientsim, 'a,b;c1;c2')


%Delay Calculations

%input pattern 
inp = [0    0 ;  0    1 ;   0    0 ;   1    0 ; ...
       0    0 ;  1    1 ;   0    1 ;   1    0 ; ...
       1    1 ;  1    0 ;   0    1 ;   1    1 ; ...
       0    0 ;];
   
inp_transition_list = ['00-01'; '01-00'; '00-10'; '10-00'; ...
                       '00-11'; '11-01'; '01-10'; '10,11'; ...
                       '11-10'; '10-01'; '01-11'; '11-00'];

% 0 => no transition +1 => pos transition -1 => neg transition 
nand_transitions = [ 0,  0,  0,  0, -1, +1, 0, -1, +1, 0, -1, +1 ];
nor_transitions =  [-1, +1, -1, +1, -1,  0, 0,  0,  0, 0,  0, +1 ];

inp_sig_nand = [evalsig(transientsim,'a1') evalsig(transientsim,'b1')] - globals.supply/2;
out_sig_nand = evalsig(transientsim,'c1') - globals.supply/2;

inp_sig_nor =  [evalsig(transientsim,'a2') evalsig(transientsim,'b2')] - globals.supply/2;
out_sig_nor = evalsig(transientsim,'c2') - globals.supply/2;

delay = zeros(N,2);

for i=1:N
    begintime = (i)*(T - 0.2e-9);
    bidx = getElementNumber(time,begintime);
    endtime = (i+1)*(T - 0.2e-9);
    eidx = getElementNumber(time,endtime);
    
    tr_type = inp(i+1,:)-inp(i,:);
    
    if(tr_type(1) == 1)
      tra1 = findPositiveZeroCrossings(time(bidx:eidx),inp_sig_nand(bidx:eidx,1));
      tra2 = findPositiveZeroCrossings(time(bidx:eidx),inp_sig_nor(bidx:eidx,1));
    elseif(tr_type(1) == -1)
      tra1 = findNegativeZeroCrossings(time(bidx:eidx),inp_sig_nand(bidx:eidx,1));
      tra2 = findNegativeZeroCrossings(time(bidx:eidx),inp_sig_nor(bidx:eidx,1));
    else
      tra1 =Inf; tra2 =Inf;
    end
    
    if(tr_type(2) == 1)
      trb1 = findPositiveZeroCrossings(time(bidx:eidx),inp_sig_nand(bidx:eidx,2));
      trb2 = findPositiveZeroCrossings(time(bidx:eidx),inp_sig_nor(bidx:eidx,2));
    elseif(tr_type(2) == -1)
      trb1 = findNegativeZeroCrossings(time(bidx:eidx),inp_sig_nand(bidx:eidx,2));
      trb2 = findNegativeZeroCrossings(time(bidx:eidx),inp_sig_nor(bidx:eidx,2));
    else
      trb1 = Inf; trb2 = Inf;
    end   
    inp_tr_time1 = min(tra1,trb1);
    inp_tr_time2 = min(tra2,trb2);
    
    if(nand_transitions(i) == 1)
        delay(i,1) = findPositiveZeroCrossings(time(bidx:eidx),out_sig_nand(bidx:eidx,1)) - inp_tr_time1;
    elseif(nand_transitions(i) == -1)
        delay(i,1) = findNegativeZeroCrossings(time(bidx:eidx),out_sig_nand(bidx:eidx,1)) - inp_tr_time1;
    else
        delay(i,1) = 0;
    end
    
    if(nor_transitions(i) == 1)
        delay(i,2) = findPositiveZeroCrossings(time(bidx:eidx),out_sig_nor(bidx:eidx,1)) - inp_tr_time2;
    elseif(nor_transitions(i) == -1)
        delay(i,2) = findNegativeZeroCrossings(time(bidx:eidx),out_sig_nor(bidx:eidx,1)) - inp_tr_time2;
    else
        delay(i,2) = 0;
    end
    
end


disp('Delay (ps)');
    fprintf('      NAND   :  NOR   \n');
for i=1:N
    fprintf('%s: ',string(inp_transition_list(i,:)));
        
    fprintf(' %03.1f  : %03.1f',delay(i,1)*1e12,delay(i,2)*1e12);     
    
    fprintf('\n')
end

disp(['Max Delay through  (ps)']);
fprintf(' %f ',max(delay)*1e12);     

disp(' ')



