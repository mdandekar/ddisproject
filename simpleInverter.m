%% Setup and run the simulation
close all
clear all
clear transientsim
clear acsim
inputfile = 'simpleInverter';

globals.load_size = 4;
supplyarr = [1];%[1.2:-0.01:0.7];
delay = zeros(length(supplyarr),1);



for i=1:length(supplyarr)
    globals.supply = supplyarr(i);

    runSpice;

%% Plot the output and calculate the delay
%{    
    if(i==1)
      plotsig(transientsim, 'in,out1,out2');  %The transient simulation is stored in transientsim, make a plot
    else
      plotsig(transientsim, 'out1,out2');  %The transient simulation is stored in transientsim, make a plot  
    end
%}    
    time        = evalsig(transientsim, 'TIME');
    n1          = evalsig(transientsim, 'in');
    n2          = evalsig(transientsim, 'out1');
    n1Crossing  = findPositiveZeroCrossings(time, n1 - globals.supply/2); %Calculate the time when the n1 crosses vdd/22
    n2Crossing  = findNegativeZeroCrossings(time, n2 - globals.supply/2);

    delay(i) = (n2Crossing(2:end-1) - n1Crossing(2:end-1))*1e12;
    disp(' ');
    disp(strcat(num2str(delay),' ps'))
    disp(' ');
    displayNodalCapacitance; % Show the capacitance table if calculated.
    
end
figure;
plot(supplyarr,delay,'x-b');

figure; hold on;
X = log10(supplyarr/supplyarr(21));
Y = log10(delay/delay(21));
plot(X,Y,'x-b');
H = [X' ones(length(supplyarr),1)];
AB = (H' * H)\ (H' * Y);
Y_hat = H*AB;
plot(X,Y_hat,'x-r');








