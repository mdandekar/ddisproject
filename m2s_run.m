function m2s = m2s_run(m2s,globals);function m2s_outstr = m2s_arg2str(m2s_instr);m2s_outstr = m2s_instr;for m2s_I=1:length(m2s_instr);if isnumeric(m2s_instr{m2s_I});if length(m2s_instr{m2s_I})>1m2s_outstr{m2s_I} = mat2str(m2s_instr{m2s_I},8);m2s_outstr{m2s_I} = m2s_outstr{m2s_I}(2:end-1);else;m2s_outstr{m2s_I} = num2str(m2s_instr{m2s_I},8);end;elseif islogical(m2s_instr{m2s_I});m2s_outstr{m2s_I} = num2str(m2s_instr{m2s_I});end;end;end;function m2s_outstr=m2s_cell2str(m2s_instr);m2s_outstr=cell2mat(cellfun(@(m2s_x) [m2s_x sprintf('\n')],m2s_instr,'UniformOutput',0));m2s_outstr=m2s_outstr(1:end-1);end;function varargout = m2s_emptyfn(varargin);throwAsCaller(MException('M2S:FnErr','function called before it has been initialized with $include, $insert or $import'));end;
m2s_file_DDIS_Design_Dandekar_Mohit_Adder16b_BrentKung();
function m2s_file_DDIS_Design_Dandekar_Mohit_Adder16b_BrentKung(); m2s.DDIS_Design_Dandekar_Mohit_Adder16b_BrentKung.currentline = 0;function m2s_write(m2s_format,m2s_args);m2s_args=m2s_arg2str(m2s_args);if iscell(m2s_format);m2s_format=m2s_cell2str(m2s_format);end;m2s.DDIS_Design_Dandekar_Mohit_Adder16b_BrentKung.currentline=m2s.DDIS_Design_Dandekar_Mohit_Adder16b_BrentKung.currentline+1;m2s.DDIS_Design_Dandekar_Mohit_Adder16b_BrentKung.outstr{m2s.DDIS_Design_Dandekar_Mohit_Adder16b_BrentKung.currentline}=sprintf(m2s_format,m2s_args{:});end;
m2s_write('******************************',{});
m2s_write('**** 16b Brent-Kung adder ****',{});
m2s_write('* Mohit Dandekar r0603366 ***',{});
m2s_write('******************************',{});
m2s_write('.param supply = %s',{globals.supply});
m2s_write('.param halfsupply = %s',{globals.supply/2});
m2s_write('.param gamma=''1''',{});
m2s_write('',{});
m2s_write('* Some simulation options',{});
m2s_write('*-------------------------',{});
m2s_write('.options post nomod ',{});
m2s_write('.option opts fast parhier=local',{});
m2s_write('',{});
m2s_write('.lib ''%s '' tt',{ strcat(pwd, '/Resources/Technology/tech_wrapper.lib') });
m2s_write('*.lib ''%s '' tt',{ strcat(pwd, '/Resources/Technology/tech_wrapperHP.lib') });
m2s_write('',{});
m2s_write('.tran 0.005n %s',{globals.sim_time});
m2s_write('.vec ''%s''',{ strcat(pwd, '/m2sfiles/Adder16b.vec')});
m2s_write('',{});
m2s_write('.probe i',{});
m2s_write('',{});
m2s_write('Vdd vdd vss supply',{});
m2s_write('Vdd2 vdd2 vss supply',{});
m2s_write('',{});
m2s_write('Vss vss 0 0 ',{});
m2s_write('',{});
m2s_write('* Actual circuit',{});
m2s_write('*----------------',{});
 n = 16;
 for j = 0:n-1
m2s_write('    xNOTa%s  a%s  aN%s     vdd vss MYNOT',{j,j,j});
m2s_write('    xNOTaN%s aN%s a_buff%s vdd vss MYNOT',{j,j,j});
m2s_write('    xNOTb%s  b%s  bN%s     vdd vss MYNOT',{j,j,j});
m2s_write('    xNOTbN%s bN%s b_buff%s vdd vss MYNOT',{j,j,j});
 end
m2s_write('',{});
m2s_write('',{});
m2s_write('Xadder %s %s %s vdd vss ADDER',{xbus('a_buff',0:15),xbus('b_buff',0:15),xbus('s',0:16)});
m2s_write('',{});
m2s_write('   ',{});
 for i = 1:16
m2s_write('    xNOT%s  s%s sN%s vdd2 vss MYNOT multfac = 16',{i,i,i});
 end
m2s_write('',{});
m2s_write('   ',{});
m2s_write('',{});
m2s_write('* Brent-Kung Adder subcircuit',{});
m2s_write('*-----------------------------',{});
m2s_write('.SUBCKT ADDER a0 a1 a2 a3 a4 a5 a6 a7 a8 a9 a10 a11 a12 a13 a14 a15 b0 b1 b2 b3 b4 b5 b6 b7 b8 b9 b10 b11 b12 b13 b14 b15 s0 s1 s2 s3 s4 s5 s6 s7 s8 s9 s10 s11 s12 s13 s14 s15 s16 vdd vss ',{});
m2s_write('',{});
m2s_write('  *------ Layer 1 ------',{});
m2s_write('  xGPBARNode1x0  a0  b0  genb0  propb0  vdd vss GPBAR gatesize=''1'' ',{});
m2s_write('  xGPBARNode1x1  a1  b1  genb1  propb1  vdd vss GPBAR gatesize=''1'' ',{});
m2s_write('  xGPBARNode1x2  a2  b2  genb2  propb2  vdd vss GPBAR gatesize=''1'' ',{});
m2s_write('  xGPBARNode1x3  a3  b3  genb3  propb3  vdd vss GPBAR gatesize=''1'' ',{});
m2s_write('  xGPBARNode1x4  a4  b4  genb4  propb4  vdd vss GPBAR gatesize=''1'' ',{});
m2s_write('  xGPBARNode1x5  a5  b5  genb5  propb5  vdd vss GPBAR gatesize=''1'' ',{});
m2s_write('  xGPBARNode1x6  a6  b6  genb6  propb6  vdd vss GPBAR gatesize=''1'' ',{});
m2s_write('  xGPBARNode1x7  a7  b7  genb7  propb7  vdd vss GPBAR gatesize=''1'' ',{});
m2s_write('  xGPBARNode1x8  a8  b8  genb8  propb8  vdd vss GPBAR gatesize=''1'' ',{});
m2s_write('  xGPBARNode1x9  a9  b9  genb9  propb9  vdd vss GPBAR gatesize=''1'' ',{});
m2s_write('  xGPBARNode1x10 a10 b10 genb10 propb10 vdd vss GPBAR gatesize=''1'' ',{});
m2s_write('  xGPBARNode1x11 a11 b11 genb11 propb11 vdd vss GPBAR gatesize=''1'' ',{});
m2s_write('  xGPBARNode1x12 a12 b12 genb12 propb12 vdd vss GPBAR gatesize=''1'' ',{});
m2s_write('  xGPBARNode1x13 a13 b13 genb13 propb13 vdd vss GPBAR gatesize=''1'' ',{});
m2s_write('  xGPBARNode1x14 a14 b14 genb14 propb14 vdd vss GPBAR gatesize=''1'' ',{});
m2s_write('  xGPBARNode1x15 a15 b15 genb15 propb15 vdd vss GPBAR gatesize=''1'' ',{});
m2s_write('',{});
m2s_write('  *------ Layer 2 ------',{});
m2s_write('  xDBUFFERNode2x0 propb0  genb0  prop0xbuf0  gen0xbuf0  vdd vss DBUFFERB gate_size=''1''',{});
m2s_write('  xDOTNode2x0 genb0  propb0  genb1  propb1  gen1_0   prop1_0   vdd vss DOT multfac=''1''',{});
m2s_write('',{});
m2s_write('  xDBUFFERNode2x1 propb2  genb2  prop2xbuf0  gen2xbuf0  vdd vss DBUFFERB gate_size=''1''',{});
m2s_write('  xDOTNode2x1 genb2  propb2  genb3  propb3  gen3_2   prop3_2   vdd vss DOT multfac=''1''',{});
m2s_write('',{});
m2s_write('  xDBUFFERNode2x2 propb4  genb4  prop4xbuf0  gen4xbuf0  vdd vss DBUFFERB gate_size=''1''',{});
m2s_write('  xDOTNode2x2 genb4  propb4  genb5  propb5  gen5_4   prop5_4   vdd vss DOT multfac=''1''',{});
m2s_write('',{});
m2s_write('  xDBUFFERNode2x3 propb6  genb6  prop6xbuf0  gen6xbuf0  vdd vss DBUFFERB gate_size=''1''',{});
m2s_write('  xDOTNode2x3 genb6  propb6  genb7  propb7  gen7_6   prop7_6   vdd vss DOT multfac=''1''',{});
m2s_write('',{});
m2s_write('  xDBUFFERNode2x4 propb8  genb8  prop8xbuf0  gen8xbuf0  vdd vss DBUFFERB gate_size=''1''',{});
m2s_write('  xDOTNode2x4 genb8  propb8  genb9  propb9  gen9_8   prop9_8   vdd vss DOT multfac=''1''',{});
m2s_write('',{});
m2s_write('  xDBUFFERNode2x5 propb10 genb10 prop10xbuf0 gen10xbuf0 vdd vss DBUFFERB gate_size=''1''',{});
m2s_write('  xDOTNode2x5 genb10 propb10 genb11 propb11 gen11_10 prop11_10 vdd vss DOT multfac=''1''',{});
m2s_write(' ',{});
m2s_write('  xDBUFFERNode2x6 propb12 genb12 prop12xbuf0 gen12xbuf0 vdd vss DBUFFERB gate_size=''1''',{});
m2s_write('  xDOTNode2x6 genb12 propb12 genb13 propb13 gen13_12 prop13_12 vdd vss DOT multfac=''1''',{});
m2s_write('',{});
m2s_write('  xDBUFFERNode2x7 propb14 genb14 prop14xbuf0 gen14xbuf0 vdd vss DBUFFERB gate_size=''1''',{});
m2s_write('  xDOTNode2x7 genb14 propb14 genb15 propb15 gen15_14 prop15_14 vdd vss DOT multfac=''1''',{});
m2s_write('',{});
m2s_write('',{});
m2s_write('  *------ Layer 3 ------',{});
m2s_write('  xDBUFFERNode3x0 prop1_0   gen1_0   propb1_0xbuf0   genb1_0xbuf0   vdd vss DBUFFERB gate_size=''1''',{});
m2s_write('  ',{});
m2s_write('  xDBUFFERNode3x1 prop2xbuf0  gen2xbuf0 propb2xbuf1   genb2xbuf1   vdd vss DBUFFERB gate_size=''1''',{});
m2s_write('',{});
m2s_write('  xDOTBARNode3x0 gen1_0   prop1_0   gen3_2   prop3_2   genb3_0   propb3_0   vdd vss DOTBAR multfac=''1''',{});
m2s_write('',{});
m2s_write('  xDBUFFERNode3x2 prop4xbuf0  gen4xbuf0 propb4xbuf1   genb4xbuf1   vdd vss DBUFFERB gate_size=''1''',{});
m2s_write('',{});
m2s_write('  xDBUFFERNode3x3 prop5_4   gen5_4   propb5_4xbuf0   genb5_4xbuf0   vdd vss DBUFFERB gate_size=''1''',{});
m2s_write('',{});
m2s_write('  xDBUFFERNode3x4 prop6xbuf0  gen6xbuf0 propb6xbuf1   genb6xbuf1   vdd vss DBUFFERB gate_size=''1''',{});
m2s_write('',{});
m2s_write('  xDOTBARNode3x1 gen5_4   prop5_4   gen7_6   prop7_6   genb7_4   propb7_4   vdd vss DOTBAR multfac=''1''',{});
m2s_write('',{});
m2s_write('  xDBUFFERNode3x5 prop9_8   gen9_8   propb9_8xbuf0   genb9_8xbuf0   vdd vss DBUFFERB gate_size=''1''',{});
m2s_write('',{});
m2s_write('  xDOTBARNode3x2 gen9_8   prop9_8   gen11_10 prop11_10 genb11_8  propb11_8  vdd vss DOTBAR multfac=''1''',{});
m2s_write('',{});
m2s_write('  xDBUFFERNode3x6 prop13_12 gen13_12 propb13_12xbuf0 genb13_12xbuf0 vdd vss DBUFFERB gate_size=''1''',{});
m2s_write('',{});
m2s_write('  xDOTBARNode3x3 gen13_12 prop13_12 gen15_14 prop15_14 genb15_12 propb15_12 vdd vss DOTBAR multfac=''1''',{});
m2s_write('',{});
m2s_write('  ',{});
m2s_write('  *------ Layer 4 ------',{});
m2s_write('',{});
m2s_write('  xBUFFERNode4_0 genb1_0xbuf0 gen1_0xbuf1 vdd vss MYNOT multfac=''1''',{});
m2s_write('',{});
m2s_write('  xDOTNode4x0 genb1_0xbuf0 propb1_0xbuf0 genb2xbuf1  propb2xbuf1  gen2_0  prop2_0 vdd vss DOT multfac=''1'' ',{});
m2s_write('',{});
m2s_write('  xDBUFFERNode4x0 propb3_0  genb3_0  prop3_0xbuf0  gen3_0xbuf0 vdd vss DBUFFERB gate_size=''1''',{});
m2s_write('',{});
m2s_write('  xDBUFFERNode4x1 propb5_4xbuf0   genb5_4xbuf0  prop5_4xbuf1   gen5_4xbuf1 vdd vss DBUFFERB gate_size=''1''',{});
m2s_write('  ',{});
m2s_write('  xDOTNode4x1 genb3_0  propb3_0  genb7_4   propb7_4   gen7_0  prop7_0 vdd vss DOT multfac=''1'' ',{});
m2s_write('',{});
m2s_write('  xDBUFFERNode4x2 propb11_8 genb11_8 prop11_8xbuf0 gen11_8xbuf0 vdd vss DBUFFERB gate_size=''1''',{});
m2s_write('',{});
m2s_write('  xDOTNode4x2 genb11_8 propb11_8 genb15_12 propb15_12 gen15_8 prop15_8 vdd vss DOT multfac=''1''',{});
m2s_write('',{});
m2s_write('',{});
m2s_write('  *------ Layer 5 ------',{});
m2s_write('  xDBUFFERNode5x0 prop3_0xbuf0 gen3_0xbuf0 propb3_0xbuf1 genb3_0xbuf1 vdd vss DBUFFERB gate_size=''1''',{});
m2s_write('',{});
m2s_write('  xDOTBARNode5x0 gen3_0xbuf0 prop3_0xbuf0 gen5_4xbuf1 prop5_4xbuf1  genb5_0 propb5_0 vdd vss DOTBAR multfac=''1''',{});
m2s_write('',{});
m2s_write('  xDBUFFERNode5x1 prop7_0 gen7_0 propb7_0xbuf0 genb7_0xbuf0 vdd vss DBUFFERB gate_size=''1''',{});
m2s_write('',{});
m2s_write('  xDOTBARNode5x1 gen7_0 prop7_0 gen11_8xbuf0 prop11_8xbuf0 genb11_0 propb11_0 vdd vss DOTBAR multfac=''1''',{});
m2s_write('',{});
m2s_write('  *------ Layer 6 ------  ',{});
m2s_write('',{});
m2s_write('  xBUFFERNode6_0 genb3_0xbuf1 gen3_0xbuf2 vdd vss MYNOT multfac=''1''',{});
m2s_write('',{});
m2s_write('  xDOTNode6x0 genb3_0xbuf1 propb3_0xbuf1 genb4xbuf1 propb4xbuf1  gen4_0 prop4_0 vdd vss DOT multfac=''1''',{});
m2s_write('',{});
m2s_write('  xBUFFERNode6_1 genb5_0 gen5_0xbuf0 vdd vss MYNOT multfac=''1''',{});
m2s_write('',{});
m2s_write('  xDOTNode6x1 genb5_0 propb5_0 genb6xbuf1 propb6xbuf1  gen6_0 prop6_0 vdd vss DOT multfac=''1''',{});
m2s_write('',{});
m2s_write('  xDBUFFERNode6x1 propb7_0xbuf0 genb7_0xbuf0 prop7_0xbuf1 gen7_0xbuf1 vdd vss DBUFFERB gate_size=''1''',{});
m2s_write('',{});
m2s_write('  xDOTNode6x2 genb7_0xbuf0 propb7_0xbuf0 genb9_8xbuf0 propb9_8xbuf0  gen9_0 prop9_0 vdd vss DOT multfac=''1''',{});
m2s_write('',{});
m2s_write('  xDBUFFERNode6x2 propb11_0 genb11_0 prop11_0xbuf0 gen11_0xbuf0 vdd vss DBUFFERB gate_size=''1''',{});
m2s_write('',{});
m2s_write('  ',{});
m2s_write('  xDOTBARNode6x3 genb11_0 propb11_0 genb13_12xbuf0 propb13_12xbuf0 gen13_0 prop13_0 vdd vss DOT multfac=''1''  ',{});
m2s_write(' ',{});
m2s_write('',{});
m2s_write('  *------ Layer 7 ------  ',{});
m2s_write('  ',{});
m2s_write('  xBUFFERNode7_0 gen7_0xbuf1 genb7_0xbuf2 vdd vss MYNOT multfac=''1''',{});
m2s_write('  ',{});
m2s_write('  xDOTBARNode7_0 gen7_0xbuf1 prop7_0xbuf1 gen8xbuf0 prop8xbuf0 genb8_0 propb8_0 vdd vss DOTBAR multfac=''1''  ',{});
m2s_write('',{});
m2s_write('  xBUFFERNode7_1 gen9_0 genb9_0xbuf0 vdd vss MYNOT multfac=''1''',{});
m2s_write('',{});
m2s_write('  xDOTBARNode7_1 gen9_0 prop9_0 gen10xbuf0 prop10xbuf0 genb10_0 propb10_0 vdd vss DOTBAR multfac=''1''  ',{});
m2s_write('',{});
m2s_write('  xBUFFERNode7_2 gen11_0xbuf0 genb11_0xbuf1 vdd vss MYNOT multfac=''1''',{});
m2s_write('',{});
m2s_write('  xDOTBARNode7_2 gen11_0xbuf0 prop11_0xbuf0 gen12xbuf0 prop12xbuf0 genb12_0 propb12_0 vdd vss DOTBAR multfac=''1''  ',{});
m2s_write('',{});
m2s_write('  xBUFFERNode7_3 gen13_0 genb13_0xbuf0 vdd vss MYNOT multfac=''1''',{});
m2s_write('',{});
m2s_write('  xDOTBARNode7_3 gen13_0 prop13_0 gen14xbuf0 prop14xbuf0 genb14_0 propb14_0 vdd vss DOTBAR multfac=''1''  ',{});
m2s_write('',{});
m2s_write('  xDOTBARNode7_4 gen7_0xbuf1 prop7_0xbuf1 gen15_8 prop15_8 genb15_0 propb15_0 vdd vss DOTBAR multfac=''1''  ',{});
m2s_write('   ',{});
m2s_write('',{});
m2s_write('  *------ Layer 8 ------  ',{});
m2s_write('  xSUM_0  a0  b0                 s0  vdd vss MYXOR',{});
m2s_write('  xSUM_1  a1  b1  gen0xbuf0      s1  vdd  vss SUM',{});
m2s_write('  xSUM_2  a2  b2  gen1_0xbuf1    s2  vdd  vss SUM',{});
m2s_write('  xSUM_3  a3  b3  gen2_0         s3  vdd  vss SUM',{});
m2s_write('  xSUM_4  a4  b4  gen3_0xbuf2    s4  vdd  vss SUM',{});
m2s_write('  xSUM_5  a5  b5  gen4_0         s5  vdd  vss SUM',{});
m2s_write('  xSUM_6  a6  b6  gen5_0xbuf0    s6  vdd  vss SUM',{});
m2s_write('  xSUM_7  a7  b7  gen6_0         s7  vdd  vss SUM',{});
m2s_write('  xSUM_8  a8  b8  genb7_0xbuf2   s8  vdd  vss BSUM',{});
m2s_write('  xSUM_9  a9  b9  genb8_0        s9  vdd  vss BSUM',{});
m2s_write('  xSUM_10 a10 b10 genb9_0xbuf0   s10 vdd  vss BSUM',{});
m2s_write('  xSUM_11 a11 b11 genb10_0       s11 vdd  vss BSUM',{});
m2s_write('  xSUM_12 a12 b12 genb11_0xbuf1  s12 vdd  vss BSUM',{});
m2s_write('  xSUM_13 a13 b13 genb12_0       s13 vdd  vss BSUM',{});
m2s_write('  xSUM_14 a14 b14 genb13_0xbuf0  s14 vdd  vss BSUM',{});
m2s_write('  xSUM_15 a15 b15 genb14_0       s15 vdd  vss BSUM',{});
m2s_write('  xSUM_16         genb15_0       s16 vdd vss MYNOT',{});
m2s_write('  ',{});
m2s_write('.ENDS ADDER',{});
m2s_write('',{});
m2s_write('',{});
m2s_write('',{});
m2s_write('',{});
m2s_write('* Other subcircuits',{});
m2s_write('',{});
m2s_write('*-------------------',{});
m2s_write('.SUBCKT MYNAND inp_a inp_b out_c vdd vss multfac=''1''',{});
m2s_write('',{});
m2s_write('  xM1 n1 inp_a vss vss MOSN w=''multfac*2*120e-9'' l=''45e-9''',{});
m2s_write('  xM2 out_c inp_b n1 vss MOSN w=''multfac*2*120e-9'' l=''45e-9''',{});
m2s_write('',{});
m2s_write('  xM3 out_c inp_a vdd vdd MOSP w=''multfac*gamma*120e-9'' l=''45e-9'' ',{});
m2s_write('  xM4 out_c inp_b vdd vdd MOSP w=''multfac*gamma*120e-9'' l=''45e-9'' ',{});
m2s_write('',{});
m2s_write('.ENDS MYNAND',{});
m2s_write('',{});
m2s_write('',{});
m2s_write('*-------------------',{});
m2s_write('.SUBCKT MYNOR inp_a inp_b out_c vdd vss multfac=''1''',{});
m2s_write('  ',{});
m2s_write('      xM1 out_c inp_a vss vss MOSN w=''multfac*120e-9'' l=''45e-9''',{});
m2s_write('      xM2 out_c inp_b vss vss MOSN w=''multfac*120e-9'' l=''45e-9''',{});
m2s_write('',{});
m2s_write('      xM3 n1 inp_a vdd vdd MOSP w=''multfac*gamma*2*120e-9'' l=''45e-9''',{});
m2s_write('      xM4 out_c inp_b n1 vdd MOSP w=''multfac*gamma*2*120e-9'' l=''45e-9''',{});
m2s_write('',{});
m2s_write('.ENDS MYNOR',{});
m2s_write('',{});
m2s_write('',{});
m2s_write('',{});
m2s_write('*-------------',{});
m2s_write('.SUBCKT MYXNOR inp_a inp_b out_c vdd vss multfac=''1''',{});
m2s_write('',{});
m2s_write('     xM1 n12 inp_a vss vss MOSN w=''multfac*1*2*120e-9'' l=''45e-9''',{});
m2s_write('     xM2 n12 inp_a vdd vdd MOSP w=''multfac*2*gamma*120e-9'' l=''45e-9''',{});
m2s_write('',{});
m2s_write('     xM3 n34 inp_b vss vss MOSN w=''multfac*1*2*120e-9'' l=''45e-9''',{});
m2s_write('     xM4 n34 inp_b vdd vdd MOSP w=''multfac*2*gamma*120e-9'' l=''45e-9''',{});
m2s_write('',{});
m2s_write('     xM5 out_c n34 n12 vss MOSN w=''multfac*1*2*120e-9'' l=''45e-9''',{});
m2s_write('     xM6 out_c n12 n34 vss MOSN w=''multfac*1*2*120e-9'' l=''45e-9''',{});
m2s_write('',{});
m2s_write('     xM7 out_c n34 n78 vdd MOSP w=''multfac*2*gamma*120e-9'' l=''45e-9''',{});
m2s_write('     xM8 n78 n12 vdd vdd MOSP w=''multfac*2*gamma*120e-9'' l=''45e-9''',{});
m2s_write('',{});
m2s_write('     xM9 out_c inp_b n910 vdd MOSP w=''multfac*1*gamma*120e-9'' l=''45e-9''',{});
m2s_write('     xM10 n910 inp_a vdd vdd MOSP w=''multfac*1*gamma*120e-9'' l=''45e-9''',{});
m2s_write('',{});
m2s_write('.ENDS MYXNOR',{});
m2s_write('',{});
m2s_write('',{});
m2s_write('*-------------',{});
m2s_write('.SUBCKT MYXOR inp_a inp_b out_c vdd vss multfac=''1''',{});
m2s_write('',{});
m2s_write('     xM1 n12 inp_a vdd vdd MOSP w=''multfac*2*gamma*120e-9'' l=''45e-9''',{});
m2s_write('     xM2 n12 inp_a vss vss MOSN w=''multfac*2*1*120e-9'' l=''45e-9''',{});
m2s_write('',{});
m2s_write('     xM3 n34 inp_b vdd vdd MOSP w=''multfac*2*gamma*120e-9'' l=''45e-9''',{});
m2s_write('     xM4 n34 inp_b vss vss MOSN w=''multfac*2*1*120e-9'' l=''45e-9''',{});
m2s_write('',{});
m2s_write('     xM5 out_c n34 n12 vdd MOSP w=''multfac*2*gamma*120e-9'' l=''45e-9''',{});
m2s_write('     xM6 out_c n12 n34 vdd MOSP w=''multfac*2*gamma*120e-9'' l=''45e-9''',{});
m2s_write('',{});
m2s_write('     xM7 out_c n34 n78 vss MOSN w=''multfac*2*1*120e-9'' l=''45e-9''',{});
m2s_write('     xM8 n78 n12 vss vss MOSN w=''multfac*2*1*120e-9'' l=''45e-9''',{});
m2s_write('',{});
m2s_write('     xM9 out_c inp_b n910 vss MOSN w=''multfac*1*1*120e-9'' l=''45e-9''',{});
m2s_write('     xM10 n910 inp_a vss vss MOSN w=''multfac*1*1*120e-9'' l=''45e-9''',{});
m2s_write('',{});
m2s_write('.ENDS MYXOR',{});
m2s_write('',{});
m2s_write('',{});
m2s_write('*-------------',{});
m2s_write('.SUBCKT MYXORXNOR inp_a inp_b out_c out_cc vdd vss multfac=''1''',{});
m2s_write('',{});
m2s_write('    xBuf1 inp_a inp_abufc vdd vss MYNOT multfac=''1''',{});
m2s_write('    xBuf2 inp_b inp_bbufc vdd vss MYNOT multfac=''1''',{});
m2s_write('',{});
m2s_write('    *xBuf3 inp_abufc1 inp_abufc vdd vss MYNOT multfac=''1''',{});
m2s_write('    *xBuf4 inp_bbufc1 inp_bbufc vdd vss MYNOT multfac=''1''',{});
m2s_write('',{});
m2s_write('',{});
m2s_write('    xM1 out_c inp_bbufc inp_abufc vdd MOSP w=''multfac*1*gamma*120e-9'' l = ''45e-9''',{});
m2s_write('    xM2 out_c inp_abufc inp_bbufc vdd MOSP w=''multfac*1*gamma*120e-9'' l = ''45e-9''',{});
m2s_write('',{});
m2s_write('    xM3 out_cc inp_bbufc inp_abufc vss MOSN w=''multfac*1*120e-9''      l = ''45e-9''',{});
m2s_write('    xM4 out_cc inp_abufc inp_bbufc vss MOSN w=''multfac*1*120e-9''      l = ''45e-9''',{});
m2s_write('',{});
m2s_write('',{});
m2s_write('    xM5 out_c out_cc vss vss MOSN w=''multfac*1*120e-9''        l = ''45e-9''',{});
m2s_write('    xM6 out_cc out_c vdd vdd MOSP w=''multfac*1*gamma*120e-9''  l = ''45e-9''',{});
m2s_write('',{});
m2s_write('',{});
m2s_write('    xM7 out_c inp_abufc n1 vss MOSN w=''multfac*1*120e-9''      l = ''45e-9''',{});
m2s_write('    xM8 n1 inp_bbufc vss vss   MOSN w=''multfac*1*120e-9''      l = ''45e-9''',{});
m2s_write('',{});
m2s_write('    xM9 out_cc inp_bbufc n2 vdd MOSP w=''multfac*1*gamma*120e-9'' l = ''45e-9''',{});
m2s_write('    xm10 n2 inp_abufc vdd vdd   MOSP w=''multfac*1*gamma*120e-9'' l = ''45e-9''',{});
m2s_write('',{});
m2s_write('.ENDS MYXORXNOR',{});
m2s_write('',{});
m2s_write('',{});
m2s_write('',{});
m2s_write('',{});
m2s_write('*-------------',{});
m2s_write('.SUBCKT MYMUX2X1 A B S C vdd vss multfac=''1''',{});
m2s_write(' ',{});
m2s_write('   * Invert S to make Sb',{});
m2s_write('   xM1 Sb S vss vss MOSN w=''multfac*1*1*120e-9'' l=''45e-9''',{});
m2s_write('   xM2 Sb S vdd vdd MOSP w=''multfac*1*gamma*120e-9'' l=''45e-9''',{});
m2s_write('',{});
m2s_write('   * T-Gate 1',{});
m2s_write('   xM3 Ci S  A vdd MOSP w=''multfac*1*1*120e-9'' l=''45e-9''',{});
m2s_write('   xM4 Ci Sb A vss MOSN w=''multfac*1*1*120e-9'' l=''45e-9''',{});
m2s_write('',{});
m2s_write('   * T-Gate 2',{});
m2s_write('   xM5 Ci Sb B vdd MOSP w=''multfac*1*1*120e-9'' l=''45e-9''',{});
m2s_write('   xM6 Ci S  B vss MOSN w=''multfac*1*1*120e-9'' l=''45e-9''',{});
m2s_write('',{});
m2s_write('   * Driver Buffer',{});
m2s_write('   xM7 Cib Ci vss vss MOSN w=''multfac*1*1*120e-9'' l=''45e-9''',{});
m2s_write('   xM8 Cib Ci vdd vdd MOSP w=''multfac*1*gamma*120e-9'' l=''45e-9''',{});
m2s_write('',{});
m2s_write('   xM9 C Cib vss vss MOSN w=''multfac*2*1*120e-9'' l=''45e-9''',{});
m2s_write('   xM10 C Cib vdd vdd MOSP w=''multfac*2*gamma*120e-9'' l=''45e-9''',{});
m2s_write('',{});
m2s_write('.ENDS MYMUX2X1 ',{});
m2s_write('',{});
m2s_write('',{});
m2s_write('*-------------',{});
m2s_write('.SUBCKT MYMUX2X1_1 A B S C vdd vss multfac=''1''',{});
m2s_write(' ',{});
m2s_write('',{});
m2s_write('   * Invert S to make Sb',{});
m2s_write('   xM1 Sb S vss vss MOSN w=''multfac*1*1*120e-9'' l=''45e-9''',{});
m2s_write('   xM2 Sb S vdd vdd MOSP w=''multfac*1*gamma*120e-9'' l=''45e-9''',{});
m2s_write('',{});
m2s_write('   ',{});
m2s_write('   .param alpha=''0.5''   * Assymetry parameter',{});
m2s_write('   * Arm 1',{});
m2s_write('   xM3a nu1 A  vdd vdd MOSP w=''multfac*(1/alpha)*gamma*120e-9'' l=''45e-9''',{});
m2s_write('   xM4a Cib  S nu1 vdd MOSP w=''multfac*(1/(1-alpha))*gamma*120e-9'' l=''45e-9''',{});
m2s_write('   xM5a Cib  Sb  nl1 vss MOSN w=''multfac*(1/(1-alpha))*1*120e-9'' l=''45e-9''',{});
m2s_write('   xM6a nl1 A  vss vss MOSN w=''multfac*(1/alpha)*1*120e-9'' l=''45e-9''',{});
m2s_write('',{});
m2s_write('   * Arm 2',{});
m2s_write('   xM3b nu2 B  vdd vdd MOSP w=''multfac*(1/alpha)*gamma*120e-9'' l=''45e-9''',{});
m2s_write('   xM4b Cib  Sb nu2 vdd MOSP w=''multfac*(1/(1-alpha))*gamma*120e-9'' l=''45e-9''',{});
m2s_write('   xM5b Cib  S  nl2 vss MOSN w=''multfac*(1/(1-alpha))*1*120e-9'' l=''45e-9''',{});
m2s_write('   xM6b nl2 B  vss vss MOSN w=''multfac*(1/alpha)*1*120e-9'' l=''45e-9''',{});
m2s_write('',{});
m2s_write('   * Driver Buffer',{});
m2s_write('   xbufb1 Cib C vdd vss MYNOT multfac=''2''',{});
m2s_write('   ',{});
m2s_write('.ENDS MYMUX2X1_1',{});
m2s_write('',{});
m2s_write('',{});
m2s_write('*-------------',{});
m2s_write('.SUBCKT SUM A B Cin S vdd  vss gatesize=''1''',{});
m2s_write('   xXORXNOR1 A B out outb vdd vss MYXORXNOR multfac=''gatesize*1''',{});
m2s_write('   xMUX1 out outb Cin S vdd vss MYMUX2X1 multfac=''gatesize*1''',{});
m2s_write('.ENDS SUM',{});
m2s_write('',{});
m2s_write('',{});
m2s_write('*-------------',{});
m2s_write('.SUBCKT BSUM A B Cinb S vdd vss gatesize=''1''',{});
m2s_write('   xXORXNOR1 A B out outb vdd vss MYXORXNOR multfac=''gatesize*1''',{});
m2s_write('   xMUX1 outb out Cinb S vdd vss MYMUX2X1 multfac=''gatesize*1''''',{});
m2s_write('.ENDS BSUM',{});
m2s_write('',{});
m2s_write('',{});
m2s_write('*-------------',{});
m2s_write('.SUBCKT GPBAR A B Genb Propb vdd vss gatesize=''1''',{});
m2s_write('    xNAND A B Genb  vdd vss MYNAND multfac=''gatesize*1''',{});
m2s_write('    xNOR A B Propb vdd vss MYNOR multfac=''gatesize*1''',{});
m2s_write('.ENDS GPBAR',{});
m2s_write('',{});
m2s_write('',{});
m2s_write('*-------------',{});
m2s_write('.SUBCKT DOTBAR inp_gl inp_pl inp_gu inp_pu out_gulb out_pulb vdd vss multfac=''1''',{});
m2s_write('',{});
m2s_write('    xM1 out_gulb inp_gu vss vss MOSN w=''multfac*120e-9'' l=''45e-9''',{});
m2s_write('    xM3 out_gulb inp_gl nintl vss MOSN w=''multfac*2*120e-9'' l=''45e-9''',{});
m2s_write('    xM5 nintl inp_pu vss vss MOSN w=''multfac*2*120e-9'' l=''45e-9''',{});
m2s_write('',{});
m2s_write('    xM2 nintu inp_gu vdd vdd MOSP w=''multfac*1*gamma*120e-9'' l=''45e-9''',{});
m2s_write('    xM4 out_gulb inp_gl nintu vdd MOSP w=''multfac*1*gamma*120e-9'' l=''45e-9''',{});
m2s_write('    xM6 out_gulb inp_pu nintu vdd MOSP w=''multfac*1*gamma*120e-9'' l=''45e-9''',{});
m2s_write('',{});
m2s_write('    xM7 n1 inp_pu vss vss MOSN w=''multfac*2*120e-9'' l=''45e-9''',{});
m2s_write('    xM9 out_pulb inp_pl n1 vss MOSN w=''multfac*2*120e-9'' l=''45e-9''',{});
m2s_write('',{});
m2s_write('    xM8 out_pulb inp_pu vdd vdd MOSP w=''multfac*gamma*120e-9'' l=''45e-9'' ',{});
m2s_write('    xM10 out_pulb inp_pl vdd vdd MOSP w=''multfac*gamma*120e-9'' l=''45e-9''',{});
m2s_write('',{});
m2s_write('.ENDS DOTBAR',{});
m2s_write('',{});
m2s_write('',{});
m2s_write('*-------------',{});
m2s_write('.SUBCKT DOT inp_glb inp_plb inp_gub inp_pub out_gul out_pul vdd vss multfac=''1''',{});
m2s_write('',{});
m2s_write('    xM1 nintl inp_gub vss vss MOSN w=''multfac*1*120e-9'' l=''45e-9''',{});
m2s_write('    xM3 out_gul inp_glb nintl vss MOSN w=''multfac*1*120e-9'' l=''45e-9''',{});
m2s_write('    xM5 out_gul inp_pub nintl vss MOSN w=''multfac*1*120e-9'' l=''45e-9''',{});
m2s_write('',{});
m2s_write('    xM2 out_gul inp_gub vdd vdd MOSP w=''multfac*1*gamma*120e-9'' l=''45e-9''',{});
m2s_write('    xM4 out_gul inp_glb nintu vdd MOSP w=''multfac*2*gamma*120e-9'' l=''45e-9''',{});
m2s_write('    xM6 nintu inp_pub vdd vdd MOSP w=''multfac*2*gamma*120e-9'' l=''45e-9''',{});
m2s_write('',{});
m2s_write('    xM7 out_pul inp_pub vss vss MOSN w=''multfac*1*120e-9'' l=''45e-9''',{});
m2s_write('    xM9 out_pul inp_plb vss vss MOSN w=''multfac*1*120e-9'' l=''45e-9''',{});
m2s_write('',{});
m2s_write('    xM8 n1 inp_pub vdd vdd MOSP w=''multfac*2*gamma*120e-9'' l=''45e-9'' ',{});
m2s_write('    xM10 out_pul inp_plb n1 vdd MOSP w=''multfac*2*gamma*120e-9'' l=''45e-9''',{});
m2s_write('',{});
m2s_write('.ENDS DOT',{});
m2s_write('',{});
m2s_write('',{});
m2s_write('*-------------',{});
m2s_write('.SUBCKT DBUFFERB input1 input2 output1b output2b vdd vss gate_size=''1''',{});
m2s_write('  xnot1 input1 output1b vdd vss MYNOT multfac=''gate_size*1''',{});
m2s_write('  xnot2 input2 output2b vdd vss MYNOT multfac=''gate_size*1''',{});
m2s_write('.ENDS DBUFFERB',{});
m2s_write('',{});
m2s_write('',{});
m2s_write('*-------------',{});
m2s_write('.SUBCKT BUFFER input output vdd vss gatesize=''1''',{});
m2s_write('  xinv1 input n0  vdd vss MYNOT multfac=''gatesize*1''',{});
m2s_write('  xinv2 n0 output vdd vss MYNOT multfac=''gatesize*1''',{});
m2s_write('.ENDS BUFFER',{});
m2s_write('',{});
m2s_write('',{});
m2s_write('*-------------',{});
m2s_write('.SUBCKT MYNOT input output vdd vss multfac=''1''',{});
m2s_write('    xM1 output input vss vss MOSN w=''multfac*120e-9'' l=''45e-9''',{});
m2s_write('    xM2 output input vdd vdd MOSP w=''multfac*gamma*120e-9'' l=''45e-9''',{});
m2s_write('.ENDS MYNOT',{});
m2s_write('',{});
m2s_write('',{});
m2s_write('',{});
m2s_write('',{});
m2s_write('.END',{});
m2s_write('',{});
end
end